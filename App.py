import sys


class App:
    # TODO: self parent close
    def __init__(self):
        # https://gist.github.com/hogelog/5338905
        from PySide2.QtWidgets import QApplication
        from lib.widgets.MainWidget import MainWidget
        from PySide2.QtGui import QIcon
        from PySide2.QtWidgets import QMenu
        from PySide2.QtWidgets import QSystemTrayIcon
        # Create application
        self.app = QApplication(sys.argv)
        self.app.setApplicationName("YouTubeDownloader")
        # Create form and init UI
        self.window = MainWidget('lib/widgets/main_window.ui', app=self)
        # self.window = self.widget.window
        # self.modal_add = ModalAddPlaylist('lib/widgets/modal_add_playlist.ui')
        # Set icon
        self.icon = QIcon("lib/favicon.png")
        self.window.setWindowIcon(self.icon)
        # Init menu
        menu = QMenu()
        # actionshow = menu.addAction(self.icon, "Показать/Скрыть")
        # actionshow.triggered.connect(lambda: self.window.hide() if self.window.isVisible() else self.window.show())
        exitAction = menu.addAction("Выйти")
        exitAction.triggered.connect(self.foo)
        # init tray
        self.tray = QSystemTrayIcon()
        self.tray.setIcon(self.icon)
        self.tray.hide()
        self.tray.setContextMenu(menu)
        self.tray.show()
        self.tray.setToolTip("Нажмите ПКМ")

        self.app.quitOnLastWindowClosed()
        # self.app.destroyed.connect(self.foo)
        self.tray.activated.connect(self.clickEvent)
        self.tray.messageClicked.connect(self.window.show)
        self.window.show()

    def clickEvent(self, event):
        from PySide2.QtWidgets import QSystemTrayIcon
        if event == QSystemTrayIcon.DoubleClick:
            if self.window.isVisible():
                self.hide()
            else:
                self.show()

    def foo(self):
        self.app.exec_()
        sys.exit()

    def show(self):
        self.window.show()
        # self.window.setWindowFlags(QtCore.Qt.Window)

    def hide(self):
        self.window.hide()
        self.tray.showMessage(
            "YouTubeDownloader",
            "Запущен в фоновом режиме",
            self.icon
        )
        # self.window.setWindowFlags(QtCore.Qt.Tool)

    def run(self):
        # Run Qt application main loop
        self.app.exec_()
        sys.exit()


if __name__ == '__main__':
    app = App()
    app.run()
