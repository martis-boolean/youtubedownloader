import os


THREAD_NAME = "SETTINGS"


# TODO: To json?
class Settings:
    __app_name__        = "YouTubeDownloader"
    __app_dir__         = os.path.dirname(os.path.dirname(__file__))

    # __lib__             = os.path.join(__app_dir__, "lib")
    __lib__             = os.path.dirname(__file__)
    __widgets__         = os.path.join(__lib__, "widgets")
    __settings__        = os.path.join(__lib__, "settings.json")
    __settings_dir__    = os.path.join(__lib__, "dir.json")

    __main__            = None
    __transferred__     = None
    __log_file__        = None
    __database__        = None

    settings            = None

    @classmethod
    def init(cls):
        cls.settings = cls.load_settings()
        cls.__main__ = cls.load_dir()["dir"]
        if cls.__main__ is None:
            cls.__main__ = os.path.join(cls.__app_dir__, cls.__app_name__)
            cls.save_dir()
        if not os.path.exists(cls.__main__):
            cls.__main__ = ""

        cls.__gen_main_subs()
        return cls.settings


    @classmethod
    def __gen_main_subs(cls):
        cls.__transferred__     = os.path.join(cls.__main__, "[__Перенесено__]")
        cls.__log_file__        = os.path.join(cls.__main__, "logs.txt")
        cls.__database__        = os.path.join(cls.__main__, "sqlite3.db")

    """
    ..............................................................................................................
    ................................................ AUTO ......................................................
    ..............................................................................................................
    """

    @classmethod
    def set_auto(cls, settings):
        from lib.utils import Trigger
        cls.settings = settings
        Trigger.download_auto(settings)

    @classmethod
    def get_auto(cls):
        return cls.settings

    """
    ..............................................................................................................
    ................................................ SETTINGS ......................................................
    ..............................................................................................................
    """
    @classmethod
    def save_settings(cls):
        from lib.utils import VFX
        import json
        with open(cls.__settings__, 'w') as f:
            json.dump(cls.settings, f)
        VFX.notify("Настройки сохранены", thread=THREAD_NAME)

    @classmethod
    def load_settings(cls):
        from lib.utils import VFX
        import json
        with open(cls.__settings__, 'r') as f:
            data = json.load(f)
        VFX.notify("Настройки загружены", thread=THREAD_NAME)
        return data

    """
    ..............................................................................................................
    ................................................ DIR ......................................................
    ..............................................................................................................
    """
    @classmethod
    def __validate(cls):
        import os.path as op
        return op.exists(cls.__database__) and op.exists(cls.__transferred__) and op.exists(cls.__log_file__)

    @classmethod
    def set_dir(cls, new_dir):
        res = cls.__main__
        cls.__main__ = new_dir
        cls.__gen_main_subs()
        if cls.__validate():
            cls.save_dir()
            return True
        else:
            cls.__main__ = res
            cls.__gen_main_subs()
            return False

    @classmethod
    def save_dir(cls):
        import json
        data = {"dir": cls.__main__}
        with open(cls.__settings_dir__, 'w') as f:
            json.dump(data, f)

    @classmethod
    def load_dir(cls):
        import json
        with open(cls.__settings_dir__, 'r') as f:
            data = json.load(f)
        return data