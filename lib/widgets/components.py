from PySide2.QtWidgets import QPushButton, QLabel, QLineEdit

"""
ONLY FOR COMPONENTS
"""

buttons = [
    "btn_pl_add",                   # pl - panel
    "btn_check_all",                # pl - panel
    "btn_check_none",               # pl - panel
    "btn_pl_refresh",               # pl - panel
    "btn_hk_move_up",               # hotkey: pl - panel
    "btn_hk_move_down",             # hotkey: pl - panel
    "btn_hk_remove",                # hotkey: pl - panel
    "btn_hk_check",                 # hotkey: pl - panel
    "btn_hk_info",                  # hotkey: pl - panel
    "btn_hk_deselect",              # hotkey: pl - panel
    # "btn_hide",                     # app
    # "btn_open_pl_file",             # common settings
    "btn_open_folder",              # common settings
    # "btn_open_logfile",             # common settings
    # "btn_browse_pl_file",           # common settings
    "btn_browse_folder",            # common settings
    # "btn_browse_logfile",           # common settings
    "btn_manual_download",          # download
    "btn_auto_settings",            # download
]

labels = [
    "lbl_step",                     # current step
    "lbl_u_common",                 # section
    "lbl_u_extra",                  # section
    "lbl_playlists",                # field
    "lbl_folder",                   # field
    "lbl_logfile",                  # field
    "img_dummy",                    # video view
    "video_view_group",             # video view
    "video_view_title",             # video view
    "video_view_time",              # video view
    "video_view_img",               # video view
]

linesEdit = [
    "le_pl_file",                   # field
    "le_folder",                    # field
    "le_logfile",                   # field
]

categories = [
    {"container": buttons,          "class": QPushButton},
    {"container": labels,           "class": QLabel},
    {"container": linesEdit,        "class": QLineEdit},
]

download_detail_layout = [
    "img_dummy",
    "video_view_group",
    "video_view_title",
    "video_view_time",
    "video_view_img",
]
