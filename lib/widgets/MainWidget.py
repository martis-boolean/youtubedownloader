from fios.io import console

from PySide2.QtGui import QIcon
from PySide2.QtWidgets import *
from PySide2 import QtWidgets


class MainWidget(QtWidgets.QMainWindow):
    instance = None  # singleton

    # may be: directly, without .ui
    def __init__(self, ui_file, app, parent=None):
        super(MainWidget, self).__init__(parent)
        # init instances
        self.app = app
        self.is_downloading = False
        MainWidget.set_instance(self)
        # init ui, components and buttons
        console.process("Initialization", pattern=":")
        self.__init_ui(ui_file)
        self.__init_components()
        self.__init_handlers()
        self.__init_layouts()
        # hook logic
        from lib.functions import hook_func
        console.process("Hook logic", pattern=":")
        from lib.settings import Settings
        Settings.init()
        hook_func.prepare_src(self)
        hook_func.load_playlists(self)
        self.timer = hook_func.init_timer()
        console.process("User Actions", pattern=":")
        # self.show()

    def __init_ui(self, ui_file):
        from PySide2.QtCore import QFile
        # ui_file = QFile(ui_file)
        # ui_file.open(QFile.ReadOnly)
        from lib.utils.uiloader import loadUi
        loadUi(ui_file, self)
        # self = loader.load(ui_file)
        # self.closeEvent = self.closeEvent
        # ui_file.close()

    def __init_components(self):
        from lib.widgets import components
        from lib.items.Queue import Queue

        self.components = components

        # init buttons, labels and linesEdit
        for category in self.components.categories:
            for objectName in category["container"]:
                console.notify("➥ " + objectName)
                qt_element = self.findChild(category["class"], objectName)
                setattr(self, objectName, qt_element)
        # init misc
        self.listWidget = self.findChild(QListWidget, "listWidget")
        # self.verticalScrollBar =    self.findChild(QScrollBar, "verticalScrollBar")
        self.statusBar = self.findChild(QStatusBar, "statusbar")
        self.cb_next_pl = self.findChild(QCheckBox, "cb_next_pl")
        self.progressBar = self.findChild(QProgressBar, "progressBar")

        self.queue = Queue(self)

    def __init_handlers(self):
        from lib.functions import controllers

        buttons = self.components.buttons
        for btn in buttons:
            handler_name = "handler_" + btn.replace("btn_", "")
            getattr(self, btn).clicked.connect(
                getattr(controllers, handler_name)
            )

    def __init_layouts(self):
        from lib.utils.VFX import hide_layout
        hide_layout(self.components.download_detail_layout)

    @classmethod
    def set_instance(cls, instance):
        cls.instance = instance

    @classmethod
    def get_instance(cls):
        return cls.instance

    def closeEvent(self, event):
        if self.is_downloading:
            print("Downloading :}")
            event.ignore()
        else:
            reply = QMessageBox.question(self, 'Завершение работы', "Вы уверены, что хотите выйти?", QMessageBox.Yes, QMessageBox.No)
            if reply == QMessageBox.Yes:
                self.timer.stop()
                event.accept()
            else:
                event.ignore()

    def changeEvent(self, event):
        from PySide2.QtCore import QEvent
        from PySide2.QtCore import Qt
        if event.type() == QEvent.WindowStateChange:
            if self.windowState() & Qt.WindowMinimized:
                self.app.hide()
