import os
import sys

from PySide2.QtCore import Qt, QSize
from PySide2 import QtWidgets

from lib.items.Queue import Queue
from lib.utils import Parser, Database, VFX

BASE_DIR = os.path.dirname(__file__)
STYLE = os.path.join(BASE_DIR, "style.css")

"""
# Create the list
mylist = QListWidget()

# Add to list a new item (item is simply an entry in your list)
item = QListWidgetItem(mylist)
mylist.addItem(item)

# Instanciate a custom widget
row = MyCustomWidget()
item.setSizeHint(row.minimumSizeHint())

# Associate the custom widget to the list entry
mylist.setItemWidget(item, row)
"""


def foo():
    print("L..............................")


class PlaylistWidget(QtWidgets.QWidget):
    """ Widget for render playlists in ListWidget (Queue) """
    STYLE_CHECKED = "QPushButton { background-color: rgb(173, 0, 0) } " \
                    "QPushButton:hover { background-color: rgb(67, 67, 67);}"
    STYLE_UNCHECKED = "QPushButton { background-color: rgb(47, 47, 47) } " \
                      "QPushButton:hover { background-color: rgb(213, 0, 0);}"
    STYLE_NAME = "border: none;" \
                 "border-bottom: 1px solid rgb(145, 145, 145);" \
                 "color: #fff;" \
                 "width: 100%;" \
                 "background-color: transparent;"
    STYLE_INFO = "QPushButton {font-size: 30px;background-color: rgba(0, 0, 0, 0.5);}" \
                 "QPushButton:hover {background-color: rgba(0, 0, 0, 0.4);}"
    STYLE_TRANSPARENT = "background-color: transparent;"

    HEIGHT = 141  # TODO: 141, 130
    WIDTH = 238
    THREAD_NAME = "PL"

    # TODO: check toggle
    def __init__(self, item: QtWidgets.QListWidgetItem, queue: Queue, mode="default", parent=None):
        super(PlaylistWidget, self).__init__(parent)
        self.setObjectName("PlaylistWidget")
        """
        ..............................................................................................................
        ................................................ DATA ........................................................
        ..............................................................................................................
        """
        # init data
        self.item = item
        self.queue = queue  # TODO: Optimize
        self.playlist = item.data(Qt.UserRole)
        # init components
        # > playlist.preview
        self.preview = QtWidgets.QLabel("[IMG_PREVIEW]")
        self.preview.setFixedWidth(self.WIDTH * 0.7)
        # self.preview.setFixedSize(QSize(self.WIDTH * 0.7, self.HEIGHT * 1.0))
        VFX.setImage(component=self.preview,
                     image_url=self.playlist.pic,
                     sizes={"w": self.WIDTH * 0.7, "h": self.HEIGHT * 1.0})
        # > playlist.name
        title = self.playlist.title
        if mode == "default":
            limit = 28
            title = self.playlist.title[:limit] + "..." * (len(title) > limit)
        self.name = QtWidgets.QLabel(title)
        self.name.setStyleSheet(self.STYLE_NAME)
        # self.name.setWordWrap(True)  # TODO: wrap next line
        # > playlist.size
        self.size = QtWidgets.QLabel("[ " + str(self.playlist.size) + " ]")
        # self.size.setAlignment(Qt.AlignCenter)
        # self.size.setFixedSize(QSize(self.WIDTH * 0.3, self.HEIGHT * 0.12))
        # self.size.setStyleSheet(self.STYLE_TRANSPARENT)
        self.name.setText(self.size.text() + " " + self.name.text())

        # init buttons
        # > playlists.check()
        self.btn_check = QtWidgets.QPushButton('✓')
        self.btn_check.clicked.connect(self.handler_check)
        self.btn_check.setCursor(Qt.PointingHandCursor)
        self.btn_check.setFixedSize(QSize(self.WIDTH * 0.15, self.HEIGHT * 0.1))
        self.btn_check.setToolTip("Отметить плейлист")
        # > playlists.info()
        self.btn_info = QtWidgets.QPushButton("𝌆")
        self.btn_info.clicked.connect(self.handler_expand)
        self.btn_info.setFixedSize(QSize(self.WIDTH * 0.3, self.HEIGHT * 0.68))
        self.btn_info.setStyleSheet(self.STYLE_INFO)
        self.btn_info.setCursor(Qt.PointingHandCursor)
        self.btn_info.setToolTip("Информация о плейлисте")
        # from PySide2.QtGui import QKeySequence
        # self.btn_info.setShortcut(QKeySequence(Qt.Key_D))
        # > playlists.delete()
        self.btn_delete = QtWidgets.QPushButton("🞩")
        self.btn_delete.clicked.connect(self.handler_remove)
        self.btn_delete.setCursor(Qt.PointingHandCursor)
        self.btn_delete.setFixedSize(QSize(self.WIDTH * 0.15, self.HEIGHT * 0.1))
        self.btn_delete.setToolTip("Удалить плейлист из списка")
        # init state
        state = self.get_state(self.playlist.isChecked)
        self.setCheckState(state)
        # init signals
        # self.item.listWidget().itemDoubleClicked.connect(self.handler_expand)
        """
        ..............................................................................................................
        ................................................ LAYOUT ......................................................
        ..............................................................................................................
        """
        # init mode
        if mode == "default":
            buttons_extra = QtWidgets.QHBoxLayout()
            buttons_extra.addWidget(self.btn_check)
            buttons_extra.addWidget(self.btn_delete)

            buttons = QtWidgets.QVBoxLayout()
            buttons.addLayout(buttons_extra)
            # buttons.addWidget(self.size)
            buttons.addWidget(self.btn_info)

            # init hero window
            hero = QtWidgets.QHBoxLayout()
            hero.addWidget(self.preview)
            hero.addLayout(buttons)
        else:
            # self.size.setFixedWidth(self.WIDTH * 0.1)
            hm = 0.2
            self.name.setAlignment(Qt.AlignCenter)
            self.preview.setFixedHeight(self.HEIGHT * hm)
            self.btn_info.setFixedHeight(self.HEIGHT * hm)
            self.btn_info.setStyleSheet(self.STYLE_INFO.replace("30px", "15px"))
            self.btn_delete.setFixedSize(QSize(self.WIDTH * 0.2, self.HEIGHT * hm))

            hero = QtWidgets.QHBoxLayout()

            components = QtWidgets.QHBoxLayout()
            components.addWidget(self.preview)
            components.addWidget(self.btn_info)
            components.addWidget(self.btn_delete)

            hero.addLayout(components)

        # init container VBox
        playlist = QtWidgets.QVBoxLayout()
        playlist.addLayout(hero)
        playlist.addWidget(self.name)
        # set Layout
        self.setLayout(playlist)

        # self.show()

    """
    ..............................................................................................................
    ................................................ HANDLERS ......................................................
    ..............................................................................................................
    """

    def handler_expand(self):
        # self.item.listWidget().itemClicked.connect(foo)
        # self.item.listWidget().itemDoubleClicked.connect(self.handler_expand)
        # TODO: show videos
        # TODO: name

        # Notify message
        message = '[{}]'.format(self.playlist.title)
        VFX.notify(message, thread=self.THREAD_NAME)
        # Open modal-info
        self.playlist.more_info()

    def handler_remove(self):
        self.queue.remove(self.item, commit=True)
        message = '[{}]: удален'.format(self.playlist.title)
        VFX.notify(message, thread=self.THREAD_NAME)

    def handler_check(self):
        checked = self.item.checkState() == Qt.Checked
        self.setCheckState(self.get_state(True ^ checked), commit=True)
        if checked:
            message = '[{}]: пометка снята'.format(self.playlist.title)
        else:
            message = '[{}]: отмечен'.format(self.playlist.title)
        VFX.notify(message, thread=self.THREAD_NAME)

    """
    ..............................................................................................................
    ................................................ SET CHECK STATE .............................................
    ..............................................................................................................
    """

    def get_state(self, predicate):
        return Qt.Checked if predicate else Qt.Unchecked

    def setCheckState(self, state: Qt.CheckState, commit=False):
        isChecked = state == Qt.Checked
        # set style
        self.btn_check.setStyleSheet(self.STYLE_CHECKED if isChecked
                                     else self.STYLE_UNCHECKED)
        # set item.State
        self.item.setCheckState(state)
        # commit if need
        if commit:
            value = 1 if isChecked else 0
            Database.update(
                table="playlist",
                notify=False,
                set="isChecked = {val}".format(val=value),
                where="id = {id}".format(id=self.playlist.id),
            )


if __name__ == '__main__':
    # Create application
    app = QtWidgets.QApplication(sys.argv)
    # Create form and  init UI
    pl = Parser.get_playlist(url="https://www.youtube.com/playlist?list=PLmu6ph9LLaiJ4XM7E4HY2c_0vFvv2e8B5")
    form = PlaylistWidget(pl)
    form.show()
    # Run main_version loop
    sys.exit(app.exec_())
