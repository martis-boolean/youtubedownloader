import sys
import os.path as op
from fios.io import console

from PySide2.QtUiTools import QUiLoader
from PySide2 import QtWidgets
from PySide2.QtCore import QFile


class Window(QtWidgets.QMainWindow):
    def __init__(self, ui_file, parent=None):
        super(Window, self).__init__(parent)
        # init own_geo
        self.path = op.abspath(__file__)
        self.directory = op.dirname(self.path)
        # init ui, components and buttons
        console.notify("...INITIALIZATION...")
        self.init_ui(ui_file)
        self.init_components()

        # hook logic
        console.notify(".....HOOK LOGIC.....")
        # start app
        self.window.show()
        console.notify("....USER ACTIONS....")

    def init_ui(self, ui_file):
        ui_file = QFile(ui_file)
        ui_file.open(QFile.ReadOnly)

        loader = QUiLoader()
        self.window = loader.load(ui_file)
        ui_file.close()

    def init_components(self):
        # self.listWidget = self.window.findChild(QListWidget, "listWidget")
        pass


if __name__ == '__main__':
    # Create application
    app = QtWidgets.QApplication(sys.argv)
    # Create form and init UI
    window = Window('dummy.ui')
    # Run main_version loop
    sys.exit(app.exec_())
