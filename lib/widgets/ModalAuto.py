import sys
from PySide2.QtUiTools import QUiLoader
from PySide2.QtWidgets import *
from PySide2 import QtWidgets
from PySide2.QtCore import QFile

THREAD_NAME = "AU"


class ModalAuto(QtWidgets.QMainWindow):
    # may be: directly, without .ui
    def __init__(self, ui_file, parent=None):
        super(ModalAuto, self).__init__(parent)
        # init own_geo
        # init ui, components and buttons
        self.init_ui(ui_file)
        self.init_components()
        self.init_signals()
        # hook logic
        self.load_settings()
        # self.window.show()

    def init_ui(self, ui_file):
        ui_file = QFile(ui_file)
        ui_file.open(QFile.ReadOnly)

        loader = QUiLoader()
        self.window = loader.load(ui_file)
        ui_file.close()

        from PySide2.QtGui import QIcon
        icon = QIcon("lib/favicon.png")
        self.window.setWindowIcon(icon)

    def init_components(self):
        # Buttons
        self.btn_confirm_settings   = self.window.findChild(QPushButton, "btn_confirm_settings")

        # MISC
        self.lbl_auto_interval      = self.window.findChild(QLabel, "lbl_auto_interval")
        self.lbl_auto_state         = self.window.findChild(QLabel, "lbl_auto_state")
        self.lbl_next_check         = self.window.findChild(QLabel, "lbl_next_check")
        self.lbl_slider_interval    = self.window.findChild(QLabel, "lbl_slider_interval")
        self.slider_interval        = self.window.findChild(QSlider, 'slider_interval')
        self.radio_off              = self.window.findChild(QRadioButton, 'radio_off')
        self.radio_on               = self.window.findChild(QRadioButton, 'radio_on')
        self.listWidget             = self.window.findChild(QListWidget, "listWidget")

        # don't know why, but without that doesn't work
        from queue import Queue
        self.queue = Queue(self)

    def init_signals(self):
        self.btn_confirm_settings.clicked.connect(self.handler_confirm)
        self.slider_interval.valueChanged.connect(self.handler_slide)
    """
    ..............................................................................................................
    ................................................ HOOK LOGIC ..................................................
    ..............................................................................................................
    """

    def load_settings(self):
        from lib.settings import Settings
        settings = Settings.get_auto()
        # init interval
        self.slider_interval.setValue(settings["interval"] / 3600)
        # init state
        self.radio_on.setChecked(settings["enabled"])
    """
    ..............................................................................................................
    ................................................ HANDLERS ....................................................
    ..............................................................................................................
    """
    def handler_confirm(self):
        from lib.settings import Settings

        settings = {
            "interval": self.slider_interval.value() * 3600,
            "enabled": self.radio_on.isChecked(),
        }
        Settings.set_auto(settings)
        Settings.save_settings()
        self.window.close()

    def handler_slide(self):
        value = self.slider_interval.value()
        from lib.utils.Validator import pluralize
        message = "{i} {token}".format(
            i=value,
            token=pluralize(value, "час,часа,часов"),
        )
        self.lbl_slider_interval.setText(message)


if __name__ == '__main__':
    # Create application
    app = QApplication(sys.argv)
    # Create form and init UI
    MODAL = ModalAuto('modal_auto.ui', {})
    # Run main_version loop
    sys.exit(app.exec_())
