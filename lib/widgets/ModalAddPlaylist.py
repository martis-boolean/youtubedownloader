import sys
from PySide2.QtUiTools import QUiLoader
from PySide2.QtWidgets import *
from PySide2 import QtWidgets
from PySide2.QtCore import QFile

from lib.items.Queue import Queue
from lib.utils import Parser, VFX

THREAD_NAME = "AP"


class ModalAddPlaylist(QtWidgets.QMainWindow):
    # may be: directly, without .ui
    def __init__(self, ui_file, parent=None):
        super(ModalAddPlaylist, self).__init__(parent)
        # init own_geo
        # init ui, components and buttons
        self.init_ui(ui_file)
        self.init_components()
        self.init_signals()
        # hook logic
        # self.window.show()

    def init_ui(self, ui_file):
        ui_file = QFile(ui_file)
        ui_file.open(QFile.ReadOnly)

        loader = QUiLoader()
        self.window = loader.load(ui_file)
        ui_file.close()
        from PySide2.QtGui import QIcon
        icon = QIcon("lib/favicon.png")
        self.window.setWindowIcon(icon)

    def init_components(self):
        # Buttons
        self.btn_reload_src         = self.window.findChild(QPushButton, "btn_reload_src")
        self.btn_add_src            = self.window.findChild(QPushButton, "btn_add_src")
        self.btn_remove_all         = self.window.findChild(QPushButton, "btn_remove_all")

        self.btn_hk_remove          = self.window.findChild(QPushButton, "btn_hk_remove")
        self.btn_hk_info            = self.window.findChild(QPushButton, "btn_hk_info")
        self.btn_hk_reload          = self.window.findChild(QPushButton, "btn_hk_reload")
        # MISC
        self.le_url                 = self.window.findChild(QLineEdit, "le_url")
        self.lbl_url                = self.window.findChild(QLabel, "lbl_url")
        self.listWidget             = self.window.findChild(QListWidget, "listWidget")
        self.queue = Queue(self)

    def init_signals(self):
        self.btn_reload_src.clicked.connect(self.handler_reload_src)
        self.btn_add_src.clicked.connect(self.handler_add_src)
        self.btn_remove_all.clicked.connect(self.handler_remove_all)
        self.listWidget.model().rowsRemoved.connect(self.check_buttons)

        self.btn_hk_remove.clicked.connect(self.handler_hk_remove)
        self.btn_hk_info.clicked.connect(self.handler_hk_info)
        self.btn_hk_reload.clicked.connect(self.handler_reload_src)

    """
    ..............................................................................................................
    ................................................ HANDLERS ....................................................
    ..............................................................................................................
    """

    # TODO: DRY?
    def handler_hk_remove(self):
        item = self.queue.selectedItem()
        if item is not None:
            item["widget"].handler_remove()

    def handler_hk_info(self):
        item = self.queue.selectedItem()
        if item is not None:
            item["widget"].handler_expand()

    def check_buttons(self):
        # console.notify(self.queue.size)
        self.btn_add_src.setDisabled(self.queue.size == 0)
        self.btn_remove_all.setDisabled(self.queue.size == 0)

    def handler_reload_src(self):
        def load_process():
            url = str(self.le_url.text())
            results = []
            # validate
            message = "Некорректная ссылка"
            if Parser.is_valid_playlist(url):
                message = "Плейлист должен быть публичным и существующим!"
                parsed = Parser.get_playlist(url, public_only=True)
                if parsed is not None:
                    results.append(parsed)
                    message = "Плейлист загружен"
            elif Parser.is_valid_channel(url):
                parsed = Parser.get_channel_playlists(url)
                if parsed is not None:
                    if len(parsed) > 0:
                        results.extend(parsed)
                        message = "Плейлисты с канала загружены"
                    else:
                        message = "На канале нет доступных плейлистов :("

            # show if valid
            if results:
                self.load_playlists(results)
                self.le_url.setText("")

            VFX.notify(message, thread=THREAD_NAME)

        VFX.run_wait_process(process=load_process)
        self.check_buttons()

    def load_playlists(self, playlists):
        # TODO: load + handler_add? (different queue)
        # TODO: Remove checkstate
        added = 0
        none = 0
        for pl in playlists:
            if pl is not None:
                pl.init_extra_data(id=0, isChecked=0, priority=0)
                VFX.redraw()
                success = self.queue.add(pl, mode="compressed")
                if success:
                    added += 1
            else:
                none += 1
        from fios.io import console
        if none:
            console.notify("None amount: %s " % str(none))

        # TODO: if exists notify

    def handler_add_src(self):
        # TODO: QUEUE
        from lib.widgets.MainWidget import MainWidget
        main_widget = MainWidget.get_instance()
        added = 0
        for pl in self.queue.playlists():
            success = main_widget.queue.add(pl, commit=True)
            if success:
                added += 1
        if added > 0:
            message = "Добавлены новые плейлисты"
        else:
            message = "Плейлисты уже есть в списке"

        VFX.notify(message, thread=THREAD_NAME)
        # self.hide()
        # self.close()
        self.window.close()

    def handler_remove_all(self):
        def delete_process():
            self.queue.clear()

        VFX.run_wait_process(process=delete_process)
        self.check_buttons()


if __name__ == '__main__':
    # Create application
    app = QApplication(sys.argv)
    # Create form and init UI
    MODAL = ModalAddPlaylist('modal_add_playlist.ui')
    # MODAL.window.show()
    # Run main_version loop
    sys.exit(app.exec_())
