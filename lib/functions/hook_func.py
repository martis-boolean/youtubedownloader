"""
HOOK LOGIC FUNCTIONS
"""
import os.path as op
from PySide2.QtWidgets import QMainWindow

from lib.items.Queue import Queue
from lib.utils import Database


# STEP 1. Generate paths by __file__
def prepare_src(main_widget: QMainWindow):
    import fios.os.filemanager as fm
    from lib.settings import Settings
    # init paths
    main =          Settings.__main__
    transferred =   Settings.__transferred__
    log_file =      Settings.__log_file__
    database =      Settings.__database__
    # validate main_dir
    if main != "":
        Database.init(database)
        # fill linesEdit
        main_widget.le_folder.setText(main)

        # make elements if not exist
        fm.create_folder(main, n_length=2)
        fm.create_file(log_file, n_length=2)
        fm.create_folder(transferred, n_length=2)
    else:
        from lib.utils import VFX
        VFX.notify("Задайте корректную директорию")


# STEP 2. Load Playlists
def load_playlists(main_widget: QMainWindow):
    try:
        from PySide2.QtWidgets import QAbstractItemView
        from lib.utils import VFX
        # path = main_widget.le_pl_file.text()
        queue = main_widget.queue

        queue.clear()
        VFX.redraw()

        # TODO: mouth by?

        data = Database.read(
            table="playlist",
            notify=False,
            order_by="priority"
        )
        for item in data:
            from pytube import Playlist
            load_playlist(queue, item)

        queue.scrollTo(position=0)
        return True
    except:
        return False


def load_playlist(queue: Queue, data):
    from lib.utils import Parser
    from lib.utils import VFX
    # get playlist
    playlist = Parser.get_playlist(data["url"])
    playlist.init_extra_data(**data)
    # add to queue
    queue.add(playlist)
    VFX.redraw()
    # # process videos state
    # process_videos(playlist)


# STEP 3. Init auto-timer
def init_timer():
    from lib.utils import Trigger
    from lib.settings import Settings
    # init object
    timer = Trigger.init_timer()
    # init download auto
    Trigger.download_auto(Settings.settings)
    return timer
