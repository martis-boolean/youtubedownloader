import fios.os.filemanager as fm
from fios.io import console

from PySide2.QtCore import Qt


from lib.utils import Trigger, VFX
from lib.widgets.MainWidget import MainWidget

THREAD_NAME = "HN"
main_widget = MainWidget.get_instance()
# TODO: Controller widget?
"""
..............................................................................................................
................................................ APPLICATION .................................................
..............................................................................................................
"""


def handler_hide():
    main_widget.app.hide()


"""
..............................................................................................................
................................................ PLAYLIST PANEL ..............................................
..............................................................................................................
"""

"""
...................................................
...................... ADD NEW PLAYLIST  ..........
...................................................
"""


def handler_pl_add():
    from lib.widgets.ModalAddPlaylist import ModalAddPlaylist
    modal = ModalAddPlaylist('lib/widgets/modal_add_playlist.ui')
    modal.window.show()
    # main_widget.setDisabled(True) # TODO: rewrite!


"""
...................................................
...................... MISC  ......................
...................................................
"""


def handler_check_all():
    main_widget.queue.setStateEach(Qt.Checked)
    VFX.notify("Отмечены все плейлисты")


def handler_check_none():
    main_widget.queue.setStateEach(Qt.Unchecked)
    VFX.notify("Снято выделение со всех плейлистов")


def handler_hk_deselect():
    # TODO: Widget?
    main_widget.queue.clearSelection()


def handler_hk_move_up():
    # TODO: Widget?
    main_widget.queue.move(-1)


def handler_hk_move_down():
    # TODO: Widget?
    main_widget.queue.move(1)


# TODO: Inside Widget?
def handler_hk_remove():
    item = main_widget.queue.selectedItem()
    if item is not None:
        item["widget"].handler_remove()


# https://www.programcreek.com/python/example/108075/PyQt5.QtWidgets.QShortcut
# TODO: Inside Widget?
def handler_hk_check():
    item = main_widget.queue.selectedItem()
    if item is not None:
        item["widget"].handler_check()


# TODO: Inside Widget?
def handler_hk_info():
    item = main_widget.queue.selectedItem()
    if item is not None:
        item["widget"].handler_expand()


def handler_clear_selection():
    main_widget.queue.clearSelection()
    # VFX.notify("Выделение очищено")


def handler_pl_refresh():
    from lib.functions import hook_func
    console.process("Обновление списка", pattern="=")
    VFX.notify("Запрос на обновление плейлистов...")

    def refresh_process():
        main_widget.listWidget.clear()
        success = hook_func.load_playlists(main_widget)
        return success

    successfully = VFX.run_wait_process(refresh_process)
    message = "Список плейлистов обновлен!" if successfully else "Не удалось обновить плейлисты"
    VFX.notify(message)


"""
..............................................................................................................
................................................ DOWNLOAD SETTINGS ...........................................
..............................................................................................................
"""

"""
...................................................
...................... BROWSE *  ..................
...................................................
"""


# def handler_browse_pl_file():
#     VFX.dialog_file(caption="Открыть список плейлистов", filter_="Text Documents(*.txt)",
#                     lineEdit=main_widget.le_pl_file)
#     VFX.notify("Обновляется список плейлистов...")
#     VFX.redraw()
#     handler_pl_refresh()
#     VFX.redraw()
#

def handler_browse_folder():
    VFX.dialog_folder(caption="Открыть папку для скачивания", lineEdit=main_widget.le_folder)
    # TODO: move old structure? Create new?


# def handler_browse_logfile():
#     VFX.dialog_file(caption="Открыть файл для логов", filter_="Text Documents(*.txt)", lineEdit=main_widget.le_logfile)
#     VFX.notify("Обновлен файл для логов")


"""
...................................................
...................... OPEN *  ....................
...................................................
"""


# def handler_open_pl_file():
#     fm.open_(main_widget.le_pl_file.text())
#     VFX.notify("Открыть список плейлистов (.txt)")
#

def handler_open_folder():
    fm.open_(main_widget.le_folder.text())
    VFX.notify("Открыть главную директорию")
#
#
# def handler_open_logfile():
#     fm.open_(main_widget.le_logfile.text())
#     VFX.notify("Открыть файл с логами (.txt)")


"""
..............................................................................................................
................................................ DOWNLOAD ....................................................
..............................................................................................................
"""

"""
...................................................
.................... MANUAL DOWNLOAD  .............
...................................................
"""


def handler_manual_download():
    def download_process():
        # Get folder_save path and checked_playlists
        # path = main_widget.le_folder.text()
        from lib.settings import Settings
        path = Settings.__main__
        checked = main_widget.queue.checkedItems()
        counter = 0
        for i, pl in enumerate(checked):
            result = Trigger.download_playlist(pl, path)
            if result > 0:
                counter += 1
            elif result == -2 and len(checked) == 1:
                return -2
            # If auto-next is blocked # TODO: enable manually
            if main_widget.cb_next_pl.checkState() != Qt.Checked:
                VFX.notify("Sorry, but auto-next is blocked :(")
                break
        if len(checked) == 0:
            return -1
        return counter

    from lib.widgets import components
    path = main_widget.le_folder.text()
    import os
    if path != "" and os.path.exists(path):
        console.process("Download")
        # init download
        VFX.show_layout(components.download_detail_layout)
        # run
        main_widget.is_downloading = True
        result = download_process()
        # result = VFX.run_wait_process(process=download_process)
        if result > 0:
            VFX.notify("Плейлисты проверены и локальное хранилище обновлено!")
            console.process()
        elif result == 0:
            VFX.notify("Последняя проверка завершилась неудачно")
        elif result == -1:
            VFX.notify("Не выделен ни один плейлист!")
        elif result == -2:
            VFX.notify("Не обнаружено изменений в плейлисте")

        # init start layout
        VFX.reset_step()
        VFX.hide_layout(components.download_detail_layout)
        return result
    else:
        VFX.notify("Нужно задать директорию!")
        return 0


"""
...................................................
.................... AUTO DOWNLOAD ................
...................................................
"""


def handler_auto_settings():
    from lib.widgets.ModalAuto import ModalAuto
    # from lib.widgets.ModalAutoSettings import ModalAutoSettings
    # ModalAutoSettings('lib/widgets/modal_auto_settings.ui')
    modal = ModalAuto('lib/widgets/modal_auto.ui')
    modal.window.show()
