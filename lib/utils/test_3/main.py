#!/usr/bin/python

# Import PySide classes
import sys
from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class App:
    def __init__(self):
        # Create a Qt application
        self.app = QApplication(sys.argv)

        icon = QIcon("favicon.png")
        self.dialog = QWidget()
        self.dialog.setWindowTitle("Setting Dialog")

        self.dialog.setWindowIcon(icon)

        menu = QMenu()
        actionshow = menu.addAction("Show/Hide")
        actionshow.triggered.connect(lambda: self.dialog.hide() if self.dialog.isVisible() else self.dialog.show())
        settingAction = menu.addAction("setting")
        settingAction.triggered.connect(self.dialog.show)
        exitAction = menu.addAction("exit")
        exitAction.triggered.connect(sys.exit)

        self.dialog.tray = QSystemTrayIcon()
        self.dialog.tray.setIcon(icon)
        self.dialog.tray.setContextMenu(menu)
        self.dialog.tray.show()
        self.dialog.tray.setToolTip("Нажмите ПКМ")
        self.dialog.tray.showMessage("YouTubeDownloader", "TEST3: ажмите сюда", icon)
        self.dialog.show()

    def run(self):
        # Enter Qt application main loop
        self.app.exec_()
        sys.exit()


if __name__ == "__main__":
    app = App()
    app.run()
