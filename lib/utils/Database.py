import sqlite3
from fios.io import console

from lib.items.Playlist import Playlist
from lib.items.Video import Video

"""
..............................................................................................................
................................................ INIT ........................................................
..............................................................................................................
"""

THREAD_NAME = "DB"
connection = sqlite3.connect("")
cursor = connection.cursor()


# TODO: WHERE, SET function with kwargs
# TODO: Dyn init later...
# TODO: Dyn create table
def init(database_path):
    global connection, cursor
    connection = sqlite3.connect(database_path)
    connection.row_factory = sqlite3.Row
    cursor = connection.cursor()


# TODO: reset id
# TODO: set/where dict direct parameters
"""
..............................................................................................................
................................................ CRUD SUMMARY ................................................
..............................................................................................................
"""


def create(table, notify=True, **kwargs):
    """ Add instance to DB """
    sql = "INSERT INTO {table}({keys}) VALUES({values})".format(
        table=table,
        keys=", ".join(list(kwargs.keys())),
        values=", ".join(["?"] * len(kwargs.keys())),
    )
    cursor.execute(sql, tuple(kwargs.values()))
    connection.commit()
    if notify:
        console.notify("Created: %s" % str(kwargs), thread=THREAD_NAME)
    return cursor.lastrowid


def read(table, notify=True, **kwargs):
    """ Get instance from DB """
    sql = "SELECT * FROM {table}{where}{order_by}".format(
        table=table,
        where="" if kwargs.get("where") is None else " WHERE %s" % kwargs["where"],
        order_by="" if kwargs.get("order_by") is None else " ORDER BY %s" % kwargs["order_by"],
    )
    # print(sql)
    # console.notify(sql, thread=THREAD_NAME)
    cursor.execute(sql)
    if notify:
        console.notify("Has read: %s" % str(kwargs), thread=THREAD_NAME)
    return cursor.fetchall()


def update(table, notify=True, **kwargs):
    sql = "UPDATE {table} SET {set} WHERE {where}".format(
        table=table,
        set=kwargs["set"],
        where=kwargs["where"],
    )
    # console.notify(sql, thread=THREAD_NAME)
    cursor.execute(sql)
    connection.commit()
    if notify:
        console.notify("Updated: %s" % str(kwargs), thread=THREAD_NAME)


def delete(table, notify=True, **kwargs):
    """ Remove instance from DB """
    sql = "DELETE FROM {table} WHERE {where}".format(
        table=table,
        where=kwargs["where"],
    )
    # print(sql)
    cursor.execute(sql)
    connection.commit()
    if notify:
        console.notify("Deleted: %s" % str(kwargs), thread=THREAD_NAME)

"""
..............................................................................................................
................................................ YTD SUMMARY .................................................
..............................................................................................................
"""


def exists(table, **kwargs):
    """ If exist certain instance """
    response = list(read(table, notify=False, **kwargs))
    return len(response) > 0


def get_changes(playlist: Playlist, group=True):
    # before/after init
    before = read(
        table="video",
        notify=False,
        where="playlist = {id} AND state == 1".format(
            id=playlist.id
        )
    )
    before = set([x["url"] for x in before])
    after = set([x.url for x in playlist.videos])
    # compute added/no-changed/removed
    added =         after - before
    no_changed =    after & before
    removed =       before - after
    # # notify
    # console.notify("[+] %s" % str(added))
    # console.notify("[ ] %s" % str(no_changed))
    # console.notify("[-] %s" % str(removed))
    # package results
    if group:
        output_data = {
            "added":        get_instances(videos=playlist.videos, video_set=added),
            "no-changed":   get_instances(videos=playlist.videos, video_set=no_changed),
            "removed":      get_instances(videos=playlist.videos, video_set=removed),
        }
        return output_data
    else:
        return None


def get_instances(videos: list, video_set: set):
    new_set = []
    for video in video_set:
        response = filter_or_get(url=video, videos=videos)
        new_set.append(response)
    return new_set


def filter_or_get(url: str, videos: list):
    results = [x for x in videos if x.url == url]
    if results:
        return results[0]
    else:
        from lib.utils import Parser
        return Parser.get_video(url)


def update_state(video: Video, state: int):
    pl_id = video.playlist

    response = read(
        table="video",
        notify=False,
        where="url == '{url}' AND playlist == {pl_id}".format(
            url=video.url,
            pl_id=pl_id,
        )
    )
    if not response:
        create(
            table="video",
            notify=False,
            url=video.url,
            playlist=pl_id,
            state=state,
            title=video.title,
        )
    else:
        update(
            table="video",
            notify=False,
            set="state = {s}".format(s=state),
            where="url == '{url}' AND playlist == {pl_id}".format(
                url=video.url,
                pl_id=pl_id,
            )
        )


if __name__ == '__main__':
    # url = "https://www.youtube.com/playlist?list=PL3LQJkGQtzc4gsrFkm4MjWhTXhopsMgpv"
    # create(table="playlist", url=url, priority="3")
    # read(table="playlist", order_by="position", where="isChecked IS TRUE")
    # update(table="playlist", set="isChecked = 1", where="id IS 3")
    # delete(table="playlist", where="id == 7")

    # init(__database__)
    results = read("playlist")
    # results = read("video")
    for item in results:
        print(item)
