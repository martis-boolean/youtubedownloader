import os

from lib.utils.test.ProgressBar import ProgressBar

pb = ProgressBar(
    message="Count files",
    width=14,
)


def count_files(directory):
    files = []

    if os.path.isdir(directory):
        for path, dirs, filenames in os.walk(directory):
            files.extend(filenames)
            pb.calculateAndUpdate(len(files), 14)

    return len(files)


def make_dirs(dest):
    if not os.path.exists(dest):
        os.makedirs(dest)


def copy_files_with_progress(src, dest):
    import shutil
    count = count_files(src)

    if count > 0:
        make_dirs(dest)

        copied = 0

        for path, dirs, filenames in os.walk(src):
            for directory in dirs:
                destDir = path.replace(src, dest)
                make_dirs(os.path.join(destDir, directory))

            for sfile in filenames:
                srcFile = os.path.join(path, sfile)

                destFile = os.path.join(path.replace(src, dest), sfile)

                shutil.copy(srcFile, destFile)

                copied += 1

                pb.calculateAndUpdate(copied, count)


if __name__ == '__main__':
    directory = os.path.dirname(os.path.dirname(__file__))
    p = count_files(directory)
    print()
    print(p)
