def path(string, empty=False):
    prohibited = [
        ['\\', '∖'],
        ['/', '╱'],
        [':', '⠅'],
        ['?', '␦'],
        ['*', '⁕'],
        ['"', "''"],
        ['|', ' ⎸'],
        ['<', '≺'],
        ['>', '≻'],
        ['.', '.'],
        [',', ','],
    ]
    for char in prohibited:
        pattern = char[0]
        repl = char[1] if not empty else ""

        string = string.replace(pattern, repl)
    return string


def pluralize(value, arg="дурак,дурака,дураков"):
    args = arg.split(",")
    number = abs(int(value))
    a = number % 10
    b = number % 100

    if (a == 1) and (b != 11):
        return args[0]
    elif (a >= 2) and (a <= 4) and ((b < 10) or (b >= 20)):
        return args[1]
    else:
        return args[2]
