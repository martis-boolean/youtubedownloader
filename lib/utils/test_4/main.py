import sys
from PySide2.QtGui import QIcon
from PySide2.QtWidgets import (QApplication, QMainWindow, QMenu, QSystemTrayIcon)


class App(QMainWindow):
    def __init__(self, parent=None):
        super(App, self).__init__(parent)

        # Load UI from file
        # uic.loadUi("app.ui", self)

        # Or write your entire UI in code
        self.setFixedSize(640, 480)

        # SysTray Icon
        self.tray = QSystemTrayIcon(self)

        # Check if System supports STray icons
        icon = QIcon("favicon.png")
        self.tray.setIcon(icon)
        # self.tray.setIcon(self.windowIcon())

        # Context Menu
        ctmenu = QMenu()
        actionshow = ctmenu.addAction("Show/Hide")
        actionshow.triggered.connect(self.show_hide)
        actionquit = ctmenu.addAction("Quit")
        actionquit.triggered.connect(self.close)
        self.destroyed.connect(self.foo)
        self.tray.setContextMenu(ctmenu)
        self.tray.show()
        self.tray.showMessage("YouTubeDownloader", "TEST4: Нажмите сюда")
        # if self.tray.isSystemTrayAvailable():
        #
        # else:
        #     # Destroy unused var
        #     print("None(")
        #     self.tray = None

        # Show App
        self.show()

    def foo(self):
        print("SSSS")

    def show_hide(self):
        if self.isVisible():
            self.hide()
        else:
            self.show()


if __name__ == "__main__":
    application = QApplication(sys.argv)
    application.setApplicationName("App Name")
    application.setApplicationVersion("1.1.1.1.1")
    application.setOrganizationName("dev name")

    win = App()
    sys.exit(application.exec_())
