from time import sleep

from lib.items.RepeatedTimer import RepeatedTimer


def hello(name):
    print("Hello %s!" % name)


print("starting...")
rt = RepeatedTimer(2, hello, "World")  # it auto-starts, no need of rt.start()
if input() == "y":
    rt.start()

try:
    sleep(10)  # your long-running job goes here...
finally:
    rt.stop()  # better in a try/finally block to make sure the program ends!
