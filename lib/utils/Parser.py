import fios.web.webkit as wk

from bs4 import BeautifulSoup

from lib.items.Playlist import Playlist
from lib.items.Video import Video

# Structure?
from lib.utils import Database

PREFIX = "https://www.youtube.com"
UNAVAILABLE_PIC = "https://s.ytimg.com/yts/img/no_thumbnail-vfl4t3-4R.jpg"


# https://python-forum.io/Thread-How-to-check-if-video-has-been-deleted-or-removed-in-youtube-using-python
# TODO: Valid by GoogleApi?
# TODO: YouTube Api
def is_valid(url: str):
    import requests
    try:
        return requests.get(url).status_code == 200
    except:
        return False


def __validate_pic(pic):
    if pic.__contains__("https://"):
        return pic
    elif pic.__contains__("//s.ytimg.com"):
        return pic.replace("//s.ytimg.com", "https://s.ytimg.com")
    else:
        return PREFIX + pic


def __get_html(**kwargs):
    html = kwargs.get("html")
    url = kwargs.get("url")
    if html is None and url is not None:
        html = wk.get_html(url, notify=False)
    return html


"""
..............................................................................................................
................................................ CHANNEL .....................................................
..............................................................................................................
"""


def is_valid_channel(url: str):
    # TODO:
    return is_valid(url) and (url.count("/channel/") > 0 or url.count("/user/") > 0)
    # return url.count("/channel/") > 0


def get_channel_playlists(url: str):
    if is_valid_channel(url):
        try:
            # init soup
            playlists_page_url = url + "/" + "playlists"
            html = wk.get_html(playlists_page_url, notify=False)
            # print(html)
            soup = BeautifulSoup(html, features="html.parser")
            # init items
            items = soup.find_all("div", class_="yt-lockup-content")
            playlists = []
            print()
            for i, item in enumerate(items):
                from fios.io import console
                console.progress(i + 1, len(items))
                url = PREFIX + item.a["href"]
                pl = get_playlist(url)
                playlists.append(pl)
                # print(pl)
                # input(pl)

            return playlists
        except:
            return None
    else:
        return None


"""
..............................................................................................................
................................................ PLAYLIST ....................................................
..............................................................................................................
"""


def is_valid_playlist(url: str):
    return is_valid(url) and url.count("/playlist?list=") > 0


def is_private_playlist(**kwargs):
    html = __get_html(**kwargs)
    return html.count("Это плейлист с ограниченным доступом.") > 0


def is_exists_playlist(**kwargs):
    html = __get_html(**kwargs)
    return html.count("Плейлист не существует.") == 0


def get_playlist(url: str, public_only=False):
    # TODO: HTML instead url?
    if is_valid_playlist(url) and is_exists_playlist(url=url):
        try:
            # init soup
            html = wk.get_html(url, notify=False)
            soup = BeautifulSoup(html, features='html.parser')
            if is_private_playlist(html=html):
                if public_only:
                    return None
                else:
                    p_title = "[Плейлист с ограниченным доступом]"
                    p_videos = []
                    p_pic = UNAVAILABLE_PIC
            else:
                # init title
                full_title = soup.find("title").text
                p_title = str(full_title).replace(" - YouTube", "")
                # init pic
                p_pic = soup.find("div", class_="pl-header-thumb").img["src"]
                p_pic = __validate_pic(p_pic)
                # init videos
                table = soup.find("table", class_="pl-video-table")
                try:
                    data = table.find_all("tr", class_="pl-video")
                    p_videos = get_playlist_videos(data)
                except Exception as e:
                    p_videos = []

            return Playlist(url, p_title, p_videos, p_pic)
        except:
            return None
    else:
        return None


"""
..............................................................................................................
................................................ VIDEO .......................................................
..............................................................................................................
"""


def is_valid_video(url: str):
    return is_valid(url) and url.count("/watch?v=") > 0


def is_available_video(url: str):
    # TODO:
    import pytube
    v = pytube.YouTube(url)
    print(v)
    return True


# def is_valid_video(url: str):
def get_playlist_videos(data: list):
    try:
        videos = []
        for args in data:
            # print(args.prettify())
            td_title = args.find("td", class_="pl-video-title")
            # init url
            v_url = PREFIX + td_title.a["href"]
            v_url = clear_url(v_url)
            # init title
            v_title = str(td_title.a.text).strip()
            # # init preview
            # v_pic = args.find("span", class_="yt-thumb-clip").find("img")["data-thumb"]
            # v_pic = __validate_pic(v_pic)
            # init duration
            try:
                v_duration = args.find("div", class_="timestamp").find("span").text
            except:
                v_duration = "00:00"
            v_duration = __get_hms_format(string=v_duration)
            # init instance
            v = Video(v_url, v_title, v_duration)
            # add result
            videos.append(v)
        return videos
    except:
        return []


"""
...................................................
.................... VIDEO  .......................
...................................................
"""


def get_video(url: str):
    try:
        # TODO: itemprops?
        if is_valid_video(url):
            # init soup
            html = wk.get_html(url, notify=False)
            soup = BeautifulSoup(html, features="html.parser")
            # print(soup.prettify())
            # TODO: is_available
            # # init title
            try:
                v_title = soup.find("span", class_="watch-title").text.strip()
            except:
                v_title = "[Видео с ограниченным доступом]"
            # # init preview
            # v_pic = "https://i.ytimg.com/vi/{video_id}/maxresdefault.jpg".format(video_id=get_video_code(url))
            # # init duration
            try:
                prop = soup.find("div", class_="watch-main-col").find("meta", itemprop="duration")["content"]
                prop = prop.replace("PT", "").replace("S", "")
                prop = prop.split("M")
                seconds = int(prop[0]) * 60 + int(prop[1])
            except:
                seconds = 0
            v_duration = __get_hms_format(seconds=seconds)
            # # init instance
            return Video(url, v_title, v_duration)
        else:
            return None
    except:
        return None


def clear_url(url: str):
    return url.split("&list=")[0]


def get_video_code(url: str):
    return clear_url(url).split("watch?v=")[1]


def get_high_pic(url: str):
    def load_img(img_url):
        import urllib.request
        from PySide2.QtGui import QImage
        img = QImage()
        data = urllib.request.urlopen(img_url).read()
        img.loadFromData(data)

    # TODO: itemprops?
    #  <link href="https://i.ytimg.com/vi/{id}/maxresdefault.jpg" itemprop="thumbnailUrl"/> ?
    images = [
        "https://i.ytimg.com/vi/{video_id}/maxresdefault.jpg".format(video_id=get_video_code(url)),
        "https://i.ytimg.com/vi/{video_id}/hqdefault.jpg".format(video_id=get_video_code(url)),
        "https://i.ytimg.com/vi/D5_RLA5NJAY/maxresdefault.jpg",
    ]

    for img in images:
        try:
            load_img(img)
            return img
        except:
            continue


# TODO: to FIOS
def __get_hms_format(**kwargs):
    import time
    string = kwargs.get("string")
    seconds = kwargs.get("seconds")
    if seconds is None:
        if string is not None:
            hms = list(map(int, string.split(":")))
            if len(hms) > 2:
                seconds = hms[0] * 3600 + hms[1] * 60 + hms[2]
            else:
                seconds = hms[0] * 60 + hms[1]
    return time.strftime('%H:%M:%S', time.gmtime(seconds))


if __name__ == '__main__':
    playlists = [
        "https://www.youtube.com/playlist?list=PL-shvihPPrgiefb5UUX97dzK-2BHcm0nK",  # pics?
        # "https://www.youtube.com/playlist?list=PL-shvihPPrgjhdq8we_wzm8AI02HA16Mt",  # empty
        # "https://www.youtube.com/playlist?list=PL-shvihPPrggkhgob__xcWpMHITDtcK1d",  # private pl
        # "https://www.youtube.com/playlist?list=PL-shvihPPrgiefb5UUX97dzK-2BHcm0nK",  # unlisted pl
        # "https://www.youtube.com/playlist?list=PL3LQJkGQtzc7QGEA_97ZibuinqBmbQTqV",  # private videos
        # "https://www.youtube.com/playlist?list=PL3LQJkGQtzc55aqA3OXwM6sy-2CNvUy9u",  # not exist
        # "https://www.youtube.com/playlist?list=PL0lO_mIqDDFVnLvR39VpEtphQ8bPJ-xR9",  # public
        # "https://www.youtube.com/playlist?list=PLvoBekrlHDgQ3n8i8qLtfHanLoU9xQtSR",  # none calls
        # "https://www.youtube.com/playlisst?list=PLvoBekrlHDgQ3n8i8qLtfHanLoU9xQtSR",     # redirect to channel
        # "https://www.youtube.com/playlist?list=PL3LQJkGQtzc6n5O0gUQ73-BcMKT_MiXhB",
        # "https://www.youtube.com/playlist?list=PL3LQJkGQtzc55aqA3OXwN6sy-2CNvUy9u",
        # "https://www.youtube.com/playlist?list=PL3LQJkGQtzc6PQaRwN13dMlSMHnzTryi_",
        # "https://www.youtube.com/playlist?list=PL3LQJkGQtzc7QGEA_97ZibuinqBmbQTqV",
        # "https://www.youtube.com/playlist?list=PL3LQJkGQtzc4GFO65xRZSlg7xkA1JCuiV",
        # "https://www.youtube.com/channel/UCCyKQE_YbC1fO96Hw9XXHPQ",
        # "https://www.youtube.com/channel/UCm2Ap2wLLCG6HQLUJ7k2U_g",
        # "https://www.youtube.com/playlist?list=LL7f5bVxWsm3jlZIPDzOMcAg",
        # "https://www.youtube.com/playlist?list=PL3LQJkGQtzc4gsrFkm4MjWhTXhopsMgpv",
        # "https://www.youtube.com/playlist?list=PLvoBekrlHDgSBbONEoejZSp-RjEKv2627",
    ]

    channels = [
        # "https://www.youtube.com/channel/UC84P_AIkd52FVhZiK6P1Yfw",     # empty
        # "https://www.youtube.com/channel/UC7f5bVxWsm3jlZIPDzOMcAg",  # huge channel
        # "https://www.youtube.com/user/PlurrimiTube",  # huge channel
        # "https://www.youtube.com/channel/UCmkLpN3VaVB9K-Bc1odF0WA",     # empty channel
        # "https://www.youtube.com/user/iliakan",
        "https://www.youtube.com/user/BRIGmanMINE/featured",
        "https://www.youtube.com/channel/UCGIj-5Yy5hhs8tAbE-hbCIQ",
        "https://www.youtube.com/user/BRIGmanMINE",

        # "https://www.youtube.com/user/iliakan",
    ]

    videos = [
        "https://www.youtube.com/watch?v=LoLnvnki1eg",  # error http 403
        # "https://www.youtube.com/watch?v=09nrwDRICfI"  # private
        # 'https://www.youtube.com/watch?v=26tlmuKsJY4&t=63s',  # pic?
        # "https://www.youtube.com/watch?v=aaaaaaDOl0N&t=0s",  # unavailable
        # "https://www.youtube.com/watch?v=iXvDD_RbBsE&list=PL-shvihPPrgiefb5UUX97dzK-2BHcm0nK&index=2&t=0s",
        # "https://www.youtube.com/watch?v=COl0NBfO9w4&list=PL-shvihPPrggkhgob__xcWpMHITDtcK1d&index=2&t=d0s",
        # "https://www.youtube.com/watch?v=BbXXPlvcwYw",
        # "https://www.youtube.com/watch?v=9YY3FGkQbA0",
    ]

    times = [
        "1:15:10",
        "1:14:59",
        "45:49",
        "56:53",
    ]

    # Database.init(settings.__database__)
    # response = Database.read(
    #     table="video",
    # )
    # for item in response:
    #     r = Database.update(
    #         table="video",
    #         set="title == '{title}'".format(title=get_video(item["url"]).title),
    #         where="id == {id}".format(id=item["id"])
    #     )
    #     print(item["id"])

    for url in videos:
        # item = get_channel_playlists(url)
        # print("[{}]".format(len(item)), end=" ")
        # for x in item:
        #     try:
        #         print(x.title)
        #     except:
        #         print("!" + str(x))
        item = get_video(url)
        # item = get_playlist(url)
        # print(video)
        if item is None:
            print(item)
        else:
            print(item.content())
            print(get_high_pic(item.url))
            # for i in item:
            #     print(i.content())
                # print(get_high_pic(item.url))
            # [print(get_high_pic(x.url)) for x in item.videos]
