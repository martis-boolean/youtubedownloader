from fios.io import console

from PySide2.QtWidgets import QMessageBox

import os
from fios.os import filemanager as fm

from lib.items.Video import Video
from lib.utils import Downloader, VFX

THREAD_NAME = "TG"

"""
..............................................................................................................
................................................ DOWNLOAD ....................................................
..............................................................................................................
"""
"""
...................................................
..................... Auto ........................
...................................................
"""
from lib.items.RepeatedTimer import RepeatedTimer
rt = None  # it auto-starts, no need of rt.start()

# if rt.is_running:
#     rt.stop()
# else:
#     rt.start()

def init_timer():
    from lib.functions.controllers import handler_pl_refresh

    global rt
    # TODO: callback?
    # TODO: Stop when close
    rt = RepeatedTimer(999999, run_auto)
    return rt
    # print(__get_time(3))


def download_auto(settings):
    if settings["enabled"]:
        console.notify("Авто-проверка включена")
        rt.update_interval(settings["interval"])
        rt.start()
    else:
        console.notify("Авто-проверка выключена")
        rt.stop()


def run_auto():
    # from lib.widgets.MainWidget import MainWidget
    # main_widget = MainWidget.get_instance()
    THREAD_NAME = "AUTO-CHECKER"
    from lib.functions.controllers import handler_manual_download
    # init process
    console.process(THREAD_NAME, pattern=":")
    console.notify("Автоматическая проверка плейлистов...", thread=THREAD_NAME)
    # start checking
    result = handler_manual_download()
    # analyze results and notify
    if result > 0:
        message = "Проверено и обновлено {n} плейлистов".format(n=result)
    elif result == 0:
        message = "Не удалось обновить плейлисты"
    else:
        message = "Не выделен ни один плейлист!"
    # notify conclusion
    console.notify(message, thread=THREAD_NAME)
    # notify next checking
    console.notify("Следующая проверка в {time}".format(
        time=__get_time(rt.interval)
    ), thread=THREAD_NAME)
    console.notify("")


def __get_time(seconds_offset: int):
    from datetime import datetime
    import time
    # init now
    now = datetime.now()
    # count seconds
    # before = now.timestamp()
    before = now.hour * 3600 + now.minute * 60 + now.second
    after = before + seconds_offset
    # init future
    future = time.strftime('%H:%M:%S', time.gmtime(after))
    return future


"""
...................................................
.................... Manually .....................
...................................................
"""


# lbl_ path?
def download_playlist(pl, path):
    # preparing ...
    # TODO: VFX.playlist_focus(pl)
    # TODO: do do    do SHOW RESULTS AFTER DOWNLOADING! NEXT PLAYLIST
    VFX.set_step(pl.title)
    VFX.notify('🗘 "{}"'.format(pl.title))
    VFX.redraw()
    # downloading ...
    response = Downloader.download_playlist(pl, path)
    actual      = response["added"][0] + response["removed"][0]
    expected    = response["added"][1] + response["removed"][1]
    # show results ...
    # TODO: VFX.reset_playlist(pl)
    if expected == 0:
        return -2
    # message = '{result} [{title}]'.format(
    #     result='✔' if result else '↯',
    #     title=pl.title,
    # )
    return actual


"""
..............................................................................................................
................................................ VIDEO STATE .................................................
..............................................................................................................
"""


def add(video: Video):
    from lib.utils import Database
    # download video
    result = Downloader.download_video(video, video.playlist_instance.path[1])
    # if were transferred => clear
    if video.state == 2:
        fm.remove(video.path)
        if not video.playlist_instance.is_some_transferred:
            fm.remove(video.playlist_instance.path[2])

    # notify and update states
    if result > 0:
        # <1>
        if result == 1:
            Database.update_state(video, state=1)
            console.notify("[+] {}".format(video.title), thread=THREAD_NAME)
        return 1
    else:
        # <0>
        if result == 0:
            Database.update_state(video, state=0)
            console.notify("[↯] VideoUnavailable: {}".format(video.title), thread=THREAD_NAME)
        return 0


def remove(video: Video):
    from lib.utils import Database
    # TODO: If already removed manually - then ... ?? забить?
    # TODO: try
    # init paths
    src = video.path
    dst = video.playlist_instance.path[2]
    # move to __transferred__
    result = fm.move(src, dst, on_progress_call_back=VFX.remove_progress)
    # notify
    if result:
        # <2>
        Database.update_state(video, state=2)
        console.notify("[-] {}".format(video.title), thread=THREAD_NAME)
        return 1
    else:
        console.notify("[↝] Не удалось удалить {}".format(video.title), thread=THREAD_NAME)
        return 0


# TODO: Independent
def backup(video: Video, backup_data):
    from lib.utils import Parser
    try:
        # init backup_video
        backup_video = Parser.get_video(backup_data["url"])
        pic = Parser.get_high_pic(video.url)
        backup_video.init_extra_data(**{**backup_data, "group": 1, "pic": pic})
        # init video title
        video.title = backup_video.title
        # init paths
        src = backup_video.path
        dst = video.playlist_instance.path[1]
        # move to pl.dir
        result = fm.copy(src, dst, on_progress_call_back=VFX.backup_progress)
    except Exception as e:
        print(e)
        result = False

    # notify
    from lib.utils import Database
    if result:
        Database.update_state(video, state=1)
        console.notify("[⇓] {}".format(video.title), thread=THREAD_NAME)
        return 2
    else:
        Database.update_state(video, state=0)
        console.notify("[↭] Не удалось восстановить бэк-ап плейлиста {}".format(video.title), thread=THREAD_NAME)
        return -1
