import time
import urllib.request
from fios.io import console
from typing import cast

from PySide2 import QtCore
from PySide2.QtCore import Qt
from PySide2.QtGui import QCursor, QImage, QPixmap
from PySide2.QtWidgets import QFileDialog, QLineEdit, QApplication

# from lib.widgets.components import main_widget
from lib.widgets.MainWidget import MainWidget

main_widget = MainWidget.get_instance()

"""
..............................................................................................................
................................................ APPLICATION .................................................
..............................................................................................................
"""


# TODO: VFX manage all? main_wiget only for one class?

def set_step(step_name):
    main_widget.lbl_step.setText(step_name)


def reset_step():
    set_step("Настройка скачивания")


def set_style(component, style_file):
    import fios.io.reader as reader
    # init style
    style = reader.read(style_file)
    # set style
    component.setStyleSheet(style)


def setImage(component, image_url, sizes=None):
    """ Set image for component """
    try:
        # init component sizes
        if sizes is None:
            w = component.width()
            h = component.height()
        else:
            w = sizes["w"]
            h = sizes["h"]
        # init img data
        # print(image_url)
        img = QImage()
        data = urllib.request.urlopen(image_url).read()
        img.loadFromData(data)
        # img img_view
        imgmap = QPixmap(img).scaled(w, h, Qt.KeepAspectRatio)
        component.setPixmap(imgmap)
    except:
        unavailable = "https://i.ytimg.com/vi/D5_RLA5NJAY/maxresdefault.jpg"
        setImage(component, unavailable, sizes)


def redraw():
    """ Redraw GUI of application """
    QtCore.QCoreApplication.processEvents()


def notify(message, thread=""):
    """ Notify user about status of last operation """
    main_widget.statusBar.showMessage(message)
    if thread:
        console.notify(message, thread=thread)


"""
...................................................
.................... CURSORS  .....................
...................................................
"""


def set_wait_cursor():
    """ Set wait cursor in App """
    QApplication.setOverrideCursor(QCursor(Qt.WaitCursor))


def reset_cursor():
    """ Reset cursor in App """
    QApplication.setOverrideCursor(QCursor(Qt.ArrowCursor))


"""
...................................................
.................... PROCESSES  ...................
...................................................
"""


def run_wait_process(process):
    """ Run process that will require waiting """
    # TODO: Reset cursor for buttons?
    # Set cursor.wait
    set_wait_cursor()
    redraw()
    # Invoke some_process
    result = process()
    # Reset cursor
    reset_cursor()
    return result


"""
..............................................................................................................
................................................ EFFECTS .....................................................
..............................................................................................................
"""

# TODO: animate effects
# TODO: loading
#  effects

# TODO: rewrite
# # pl=QUEUE.get_cur_playlist(as_playlist=True)
# def playlist_focus(pl):
#     """ Set style for playlist:focus """
#     LW = main_widget.listWidget
#     LW.setCurrentItem(pl.item)
#     highlight_style = "QListWidget::item:selected{background-color: rgb(173, 0, 0); color: #fff;}"
#     LW.setStyleSheet(highlight_style)
#
#
# def playlist_pressed(pl):
#     """ Set style for playlist:pressed """
#     LW = main_widget.listWidget
#     LW.setCurrentItem(pl.item)
#     highlight_style = "QListWidget::item:selected{background-color: rgb(173, 0, 0); color: #fff;}"
#     LW.setStyleSheet(highlight_style)
#
#
# def reset_playlist(pl):
#     """ Reset playlist style """
#     LW = main_widget.listWidget
#     LW.setStyleSheet("")


"""
..............................................................................................................
................................................ DIALOG WINDOWS ..............................................
..............................................................................................................
"""


# TODO: rewrite
# # Dialog Window Explorer
def dialog_folder(**kwargs):
    # def dialog_folder(parent=None, caption="Open folder", directory=DIRECTORIES["app"], lineEdit=""):
    from lib.settings import Settings
    parent          = kwargs.get("parent", None)
    caption         = kwargs.get("caption", "Open folder")
    directory       = kwargs.get("directory", Settings.__app_dir__)
    lineEdit        = kwargs.get("lineEdit", "")

    """ Open dialog window for select folder """
    res = Settings.__main__
    el = __dialog("folder", parent, caption, directory, "", lineEdit)
    if el == "":
        main_widget.le_folder.setText(res)
        el = res
        notify("Неверно задана папка")
    else:
        result = Settings.set_dir(el)
        if result:
            from lib.utils import Database
            Database.init(Settings.__database__)
            from lib.functions import controllers
            notify("Обновлена папка для скачивания")
            redraw()
            controllers.handler_pl_refresh()
        else:
            notify("Неверно выбрана папка")
            main_widget.le_folder.setText(res)
            el = res
    return el


# def dialog_file(parent=None, caption="Open file", directory=DIRECTORIES["app"], filter_="All Files (*.*)",
#                 lineEdit=""):
#     """ Open dialog window for select file """
#     return __dialog("file", parent, caption, directory, filter_, lineEdit)


# def __dialog(item, parent=None, caption="Open *", directory=DIRECTORIES["app"], filter_="All Files (*.*)", lineEdit=""):
def __dialog(item, parent, caption, directory, filter_, lineEdit):
    """ Open dialog window for select * """
    if item == "folder":
        el = QFileDialog().getExistingDirectory(parent, caption, directory)
    else:
        el = QFileDialog().getOpenFileName(parent, caption, directory, filter_)[0]
    count = str(el).count("/")

    if count > 2: el = str(el).replace("/", "\\", count)
    if lineEdit: cast(QLineEdit, lineEdit).setText(el)
    return el


"""
..............................................................................................................
................................................ PROGRESS BAR ................................................
..............................................................................................................
"""


# def update_progress_per(per_interval=1, maximum_iters=100):
#     """ Update progress bar """
#     from lib.widgets.MainWidget import MainWidget
#     main_widget = MainWidget.get_instance()
#
#     console.notify("Update progress bar")
#     for i in range(maximum_iters):
#         value = main_widget.progressBar.value()
#         __update_pb(value + per_interval)
#         time.sleep(per_interval / 10)


def update_progress(current, limit, process_symbol):
    # update Status
    # TODO: limit progress count?
    notify("{process_symbol} {i}/{limit} {progress}".format(
        process_symbol=process_symbol,
        i=current,
        limit=limit,
        progress=" ⬤ " * current + " ◯" * (limit - current),
    ))


def download_progress(percent):
    __update_pb(percent, download=True)


def remove_progress(done, total):
    percent_int = int(float(done) / total * 100)
    __update_pb(percent_int, reverse=True)


def backup_progress(done, total):
    percent_int = int(float(done) / total * 100)
    __update_pb(percent_int, backup=True)


def reset_progress(reverse=False):
    value = 100 if reverse else 0
    __update_pb(value, download=True)
    redraw()


def __update_pb(value, **kwargs):
    progress_bar = main_widget.progressBar
    if kwargs.get("reverse"):
        value = 100 - value
        color = "rgb(90, 90, 90)"
    elif kwargs.get("backup"):
        color = "rgb(74, 179, 167)"
    else:
        color = "rgb(173, 0, 0)"

    style = "::chunk {background-color: %s;}" % color
    progress_bar.setStyleSheet(style)
    progress_bar.setValue(value)
    redraw()


"""
..............................................................................................................
................................................ VIDEO VIEW ..................................................
..............................................................................................................
"""


def display_video(video):
    """ Display video in VideoView """
    # init components
    view_group = main_widget.video_view_group
    view_title = main_widget.video_view_title
    view_time = main_widget.video_view_time
    view_img = main_widget.video_view_img
    # :set_group
    view_group.setText(video.group_label)
    # :set_title
    view_title.setText(video.title)
    # :set_time
    view_time.setText(video.duration)
    # :set_image
    setImage(view_img, video.pic)
    # :redraw
    redraw()


"""
..............................................................................................................
................................................ LAYOUT MANAGE ...............................................
..............................................................................................................
"""


def __each_component(layout: list, action):
    """ Action with every component of layout """
    for component in layout:
        if isinstance(component, str):
            element = getattr(main_widget, component)
        else:
            element = component

        action(element)


def show_layout(layout: list):
    """ Show layout """
    __each_component(layout, action=lambda x: x.show())


def hide_layout(layout: list):
    """ Hide layout """
    __each_component(layout, action=lambda x: x.hide())
