import fios.os.filemanager as fm
import os
from fios.io import console

from lib.items.Playlist import Playlist
from lib.items.Video import Video
from lib.utils import Database

# Consts
THREAD_NAME = "DW"
# Global var
file_size = 0

"""
...................................................
.................... DOWNLOAD PLAYLIST  ...........
...................................................
"""


def download_playlist(pl, path):
    # TODO: Try download playlist?
    # TODO: High pic only for download => cache?
    from lib.utils import Trigger
    from lib.utils import VFX
    console.process('download: %s' % pl.title)
    # create download directory if not exist
    fm.create_folder(pl.path[1])
    # init counters
    added = 0
    removed = 0
    # get playlist changes
    changes = Database.get_changes(pl)
    # download added videos
    for i, video in enumerate(changes["added"]):
        result = run(
            playlist=pl,
            video=video,
            trigger=Trigger.add,
            group=1,
            i=i,
            limit=len(changes["added"]),
        )
        added += result
    # transfer removed videos
    for i, video in enumerate(changes["removed"]):
        result = run(
            playlist=pl,
            video=video,
            trigger=Trigger.remove,
            group=-1,
            i=i,
            limit=len(changes["removed"]),
        )
        removed += result
    VFX.reset_progress(reverse=True)
    console.notify("Added: {}/{}".format(added, len(changes["added"])), thread=THREAD_NAME)
    console.notify("Removed: {}/{}".format(removed, len(changes["removed"])), thread=THREAD_NAME)
    return {
        "added": [
            added,
            len(changes["added"])
        ],
        "removed": [
            removed,
            len(changes["removed"])
        ],
    }


def run(**kwargs):
    from lib.utils import VFX
    from lib.utils import Parser
    # init args
    playlist    = kwargs.get("playlist")
    video       = kwargs.get("video")
    trigger     = kwargs.get("trigger")
    group       = kwargs.get("group")
    symbol      = "⮇" if group == 1 else u'🞬'
    i           = kwargs.get("i")
    limit       = kwargs.get("limit")

    pic = Parser.get_high_pic(video.url)
    # init video
    args = compute_state(video, playlist)
    video.init_extra_data(**{**args, "group": group, "pic": pic})
    # start process
    VFX.reset_progress()
    VFX.update_progress(current=i + 1, limit=limit, process_symbol=symbol)
    VFX.display_video(video)
    result = trigger(video)
    return result


def compute_state(video: Video, playlist: Playlist):
    # get data from DB
    response = Database.read(
        table="video",
        notify=False,
        where="url == '{url}' AND playlist == {pl_id}".format(
            url=video.url,
            pl_id=playlist.id,
        )
    )
    # init args
    if not response:
        args = {"id": 0, "state": 0, "playlist": playlist.id, "title": video.title}
    else:
        args = dict(response[0])

    return args


"""
...................................................
.................... DOWNLOAD VIDEO  ..............
...................................................
"""


def download_video(video: Video, path):
    # TODO: Custom validate?
    import pytube
    from pytube.exceptions import VideoUnavailable
    # https://python-pytube.readthedocs.io/en/latest/user/quickstart.html
    # high quality: merge with audio?
    # https://yagisanatode.com/2018/03/11/how-to-create-a-simple-youtube-download-program-with-a-progress-in-python-3-with-pytube/
    # internet connection
    try:
        yt = pytube.YouTube(video.url, on_progress_callback=progress_check)
        # video = yt.streams.first()
        y_v = yt.streams.filter(progressive=True).first()
        global file_size
        file_size = y_v.filesize
        downloaded = y_v.download(path)
        directory = video.playlist_instance.path[1]
        from lib.utils import Validator
        name = Validator.path(video.title, empty=True) + ".mp4"
        renamed = os.path.join(directory, name)
        # print(downloaded)
        # print(renamed)
        if renamed != downloaded:
            os.rename(downloaded, renamed)
        return True
    except pytube.exceptions.VideoUnavailable as e:
        response = Database.read(
            table="video",
            notify=False,
            where="url == '{url}' AND state > 0".format(
                url=video.url,
                pl_id=video.playlist,
            )
        )
        if response:
            from lib.utils import Trigger
            result = Trigger.backup(video, response[0])
            return result
        else:
            return False
    except Exception as e:
        console.notify(e)
        return False


# on_progress_callback takes 4 parameters.
def progress_check(stream=None, chunk=None, file_handle=None, remaining=None):
    # https://stackoverflow.com/questions/49185538/how-to-add-progress-bar
    # https://yagisanatode.com/2018/03/11/how-to-create-a-simple-youtube-download-program-with-a-progress-in-python-3-with-pytube/
    from lib.utils import VFX
    # Gets the percentage of the file that has been downloaded.
    percent = (100 * (file_size - remaining)) / file_size
    VFX.download_progress(percent)


if __name__ == '__main__':
    import pytube
    from pytube.exceptions import VideoUnavailable
    # https://github.com/nficano/pytube/issues/392
    # https://github.com/nficano/pytube/issues/416
    # https://github.com/nficano/pytube/issues/399
    # https://github.com/nficano/pytube/issues/413
    # https://github.com/nficano/pytube/issues/399
    # url = "https://www.youtube.com/watch?v=09nrwDRICfI&list=PL3LQJkGQtzc7QGEA_97ZibuinqBmbQTqV&index=2"
    url = "https://www.youtube.com/playlist?list=PL0lO_mIqDDFVnLvR39VpEtphQ8bPJ-xR9"
    # url = "https://www.youtube.com/watch?v=BfaKBs_eQXw&list=PL0lO_mIqDDFVnLvR39VpEtphQ8bPJ-xR9&index=3&t=0s"
    # url = "https://www.youtube.com/watch?v=Ud_vrT1Mi7w&list=PL0lO_mIqDDFVnLvR39VpEtphQ8bPJ-xR9&index=5"
    # url = "https://www.youtube.com/watch?v=LoLnvnki1eg"
    # url = 'https://www.youtube.com/watch?v=pAgnJDJN4VA'
    # url = "https://www.youtube.com/embed/qIcTM8WXFjk"
    # url = "https://www.youtube.com/watch?v=irauhITDrsE"
    # url = "https://www.youtube.com/watch?v=qIcTM8WXFjk"


    try:
        yt = pytube.YouTube(url)
        videos = yt.streams.filter(subtype="mp4").all()
        [print(x) for x in videos]
        for video in videos:
            try:
                d = video.download()
                print(d)
                break
            except Exception as e:
                print(e)
                continue
    except VideoUnavailable as e:
        print(e)
