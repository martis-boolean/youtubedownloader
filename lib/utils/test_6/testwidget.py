import os
import sys

from PySide2.QtCore import Slot
from PySide2.QtWidgets import QMainWindow, QMessageBox, QApplication

SCRIPT_DIRECTORY = os.path.dirname(os.path.abspath(__file__))


class MainWindow(QMainWindow):

    def __init__(self, parent=None):
        QMainWindow.__init__(self, parent)
        from lib.utils.uiloader import loadUi
        loadUi('main_window.ui', self)

    def closeEvent(self, event):
        print("LOOO")

    @Slot(bool)
    def on_clickMe_clicked(self, is_checked):
        if is_checked:
            message = self.trUtf8(b'I am checked now.')
        else:
            message = self.trUtf8(b'I am unchecked now.')
        QMessageBox.information(self, self.trUtf8(b'You clicked me'), message)

    @Slot()
    def on_actionHello_triggered(self):
        QMessageBox.information(self, self.trUtf8(b'Hello world'),
                                self.trUtf8(b'Greetings to the world.'))


def main():
    app = QApplication(sys.argv)
    window = MainWindow()
    window.show()
    app.exec_()


if __name__ == '__main__':
    main()
