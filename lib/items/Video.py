class Video(object):
    def __init__(self, url, title, duration):
        self.url = url
        self.title = title
        self.duration = duration
        self.id = 0
        self.state = 0
        # self.isExist

    def init_extra_data(self, **kwargs):
        self.id =           kwargs["id"]                    # id in DB
        self.state =        kwargs["state"]                 # state in DB
        self.group =        kwargs["group"]                 # group (added/without-changes/removed) (1, 0, -1)
        self.pic =          kwargs["pic"]                   # high quality pic
        labels = {
            1: "[ Добавление в плейлист ]",
            0: "[ *без изменений*  ]",
            -1: "[ Удаление из плейлиста ]"
        }
        self.group_label =  labels[self.group]              # group_label
        self.playlist =     kwargs["playlist"]              # playlist parent
        self.title =        kwargs["title"] if self.title.count("ограниченным") > 0 else self.title
        # self.path = None                                    # path

    @property
    def playlist_instance(self):
        from lib.utils import Database
        from lib.utils import Parser
        response = Database.read(
            table="playlist",
            notify=False,
            where="id == {id}".format(id=self.playlist)
        )
        playlist = Parser.get_playlist(response[0]["url"])
        return playlist

    @property
    def path(self):
        # not downloaded
        if self.state == 0:
            return None
        # downloaded or transferred
        else:
            import os.path as op
            from lib.utils import Validator
            name = Validator.path(self.title, empty=True) + ".mp4"
            return op.join(self.playlist_instance.path[self.state], name)

    def __str__(self):
        return self.title

    def content(self, **kwargs):
        # TODO: Optimize? Without url?
        # TODO: "D"
        indent =    kwargs.get("indent", "")
        marker =    kwargs.get("marker", "")
        url =       kwargs.get("url", "")

        if url != "":
            url = ":\n" + indent + " " * len(marker) + " " + url
        return "{ind}{marker}[{dur}] {title}{url}".format(
            marker=marker + " " if marker != "" else marker,
            ind=indent,
            dur=self.duration,
            title=self.title,
            url=url,
        )

    def full_info(self):
        info = "{title}: {duration}\nurl: {url}\npic: {pic}".format(
            title=self.title,
            duration=self.duration,
            url=self.url,
            pic=self.pic
        )
        return info
    # is available
    # is on playlist
