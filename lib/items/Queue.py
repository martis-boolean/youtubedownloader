from fios.io import console

from PySide2.QtCore import Qt
from PySide2.QtWidgets import QListWidgetItem, QAbstractItemView

THREAD_NAME = "QU"


# TODO: Queue -> ListView, ... (multi inheritance)
class Queue(object):
    # TODO: few widgets?
    def __init__(self, main_widget):
        self.parent = main_widget
        self.listWidget = main_widget.listWidget
        # QtCore.QObject.connect(self.listWidget, QtCore.SIGNAL('dropped'), self.on_layout_changed)
        self.size = self.listWidget.count()  # TODO: remove?
        self.__init_handlers()

    """
    ..............................................................................................................
    ................................................ Inner Operations ............................................
    ..............................................................................................................
    """

    def __init_handlers(self):
        self.listWidget.setVerticalScrollMode(QAbstractItemView.ScrollPerPixel)
        # from PySide2.QtGui import QIcon, QCursor
        # QListWidget(self.listWidget).setSelectionBehavior(QAbstractItemView.selectionBehavior)
        # self.listWidget.setCursor(QCursor(r"D:\2.WORK\(C) Other\Freelance\Orders\(__WIP__)\[GUI] YoutubeDownloader\main_version\assets\pre.jpg"))
        self.listWidget.setDragDropMode(QAbstractItemView.InternalMove)
        # self.parent.lbl_step
        self.listWidget.itemDoubleClicked.connect(self.foo)
        # self.listWidget.itemClicked.connect(self.bar)
        self.listWidget.model().rowsMoved.connect(self.save_positions)
        # TODO: Common handlers

    def foo(self):
        # TODO: optimize
        # TODO: Separate
        # # expand playlist
        # item = self.listWidget.currentItem()
        # widget = self.listWidget.itemWidget(item)
        # widget.handler_expand()
        # console.notify("expand!!")
        self.clearSelection()

    def clear(self, notify=False):
        """ Clear listWidget """
        # TODO: save
        self.listWidget.clear()
        self.size = 0
        if notify:
            console.notify("Очередь очищена!", thread=THREAD_NAME)

    def setItem(self, playlist, mode):
        """ Init item with widget by PlayList data"""
        item = QListWidgetItem(self.listWidget)  # create item and init listWidget

        item.setData(Qt.UserRole, playlist)  # init playlist_data
        item.setFlags(item.flags())  # init flags
        item.setCheckState(Qt.Checked)  # TODO: check if in DB
        self.setItemWidget(item, mode)
        return item

    def setItemWidget(self, item, mode):
        from lib.widgets.PlaylistWidget import PlaylistWidget
        widget = PlaylistWidget(item=item, queue=self, mode=mode)  # init widget
        item.setSizeHint(widget.minimumSizeHint())  # init sizes
        self.listWidget.setItemWidget(item, widget)

    def save_positions(self):
        """ Save positions to DataBase """
        # TODO: Separate mode and commit after apply ordering?
        # TODO: Few views
        console.notify("Reordering...", thread=THREAD_NAME)
        for i, item in enumerate(self.items):
            from lib.utils import Database
            Database.update(
                table="playlist",
                notify=False,
                set="priority = {val}".format(val=i),
                where="id == {id}".format(id=self.playlist(item).id)
            )

    """
    ..............................................................................................................
    ................................................ Items CRUD  .................................................
    ..............................................................................................................
    """

    # TODO: add - pl, remove - item?

    def add(self, playlist, commit=False, mode="default"):
        """ Add playlist to Queue """

        if self.exists(playlist):
            message = "Плейлист {title} уже есть в списке!".format(title=playlist.title)
            console.notify(message, thread=THREAD_NAME)
            return False
        else:
            # init item
            item = self.setItem(playlist, mode)
            # adding item to ListWidget
            self.size += 1
            self.listWidget.addItem(item)
            # scroll to item
            self.scrollTo(position=-1)
            if commit:
                from lib.utils import Database
                playlist.id = Database.create(
                    table="playlist",
                    notify=False,
                    url=playlist.url,
                    priority=self.size,
                )

            message = "[+] [{title}]".format(title=playlist.title)
            console.notify(message, thread=THREAD_NAME)
            return True

    def remove(self, item: QListWidgetItem, commit=False):
        """ Remove playlist from Queue """
        from lib.utils import VFX
        ind = self.listWidget.indexFromItem(item).row()  # QModelIndex.row()
        self.size -= 1
        self.listWidget.takeItem(ind)

        if commit:
            from lib.utils import Database
            Database.delete(
                table="playlist",
                notify=False,
                where="id == {id}".format(id=self.playlist(item).id),
            )
            self.save_positions()  # TODO: optimize

        message = "Удален плейлист [{title}]".format(title=self.playlist(item).title)
        VFX.notify(message, thread=THREAD_NAME)
        return item

    """
    ..............................................................................................................
    ................................................ Items info ..................................................
    ..............................................................................................................
    """

    def exists(self, playlist):
        urls = [x.url for x in self.playlists()]
        return playlist.url in urls

    def playlist(self, item: QListWidgetItem):
        """ Get playlist from item """
        return item.data(Qt.UserRole)

    @property
    def items(self):
        """ Get listWidget items """
        lw = self.listWidget
        return [lw.item(i) for i in range(lw.count())]

    def playlists(self):
        return [self.playlist(item) for item in self.items]

    def selectedItem(self):
        """ Get selected item """
        # TODO: Row?
        item = self.listWidget.currentItem()
        if item is not None:
            return {
                "item": item,
                "title": item.text(),
                "playlist": item.data(Qt.UserRole),
                "widget": self.listWidget.itemWidget(item)
            }

    def is_something_selected(self):
        """ Is something item selected """
        return self.listWidget.currentRow() != -1

    def checkedItems(self, as_playlist=True):
        """ Get all checked items """
        # for item in self.listWidget.items():
        if as_playlist:
            checked = []
            for item in self.items:
                if item.checkState() == Qt.Checked:
                    checked.append(self.playlist(item))
            return checked
        else:
            return []

    def is_valid_row(self, row):
        return -1 < row < self.size

    """
    ..............................................................................................................
    ................................................ Interactive Actions  ........................................
    ..............................................................................................................
    """

    def scrollTo(self, position: int):
        item = self.items[position]
        self.listWidget.scrollToItem(item)

    def move(self, direction):
        before = self.listWidget.currentRow()
        after = before + direction
        if self.is_valid_row(before) and self.is_valid_row(after):
            item = self.listWidget.takeItem(before)
            self.listWidget.insertItem(after, item)
            self.setItemWidget(item, mode="default")
            self.listWidget.setCurrentItem(item)
            self.save_positions()
            # TODO: Trigger rowsMoved
            return True
        else:
            return False

    def setStateEach(self, state: Qt.CheckState):
        """ Set CheckState for each item"""
        for item in self.items:
            widget = self.listWidget.itemWidget(item)
            widget.setCheckState(state, commit=True)

    def clearSelection(self):
        """ Clear selection in ListWidget """
        self.listWidget.setCurrentRow(-1)
