class Playlist(object):
    def __init__(self, url, title, videos, pic):
        self.url = url                          # Playlist url
        self.title = title                      # Playlist name ("Watch Later", ...)
        self.videos = videos                    # Container [url_1, url_2, ...]
        self.size = len(videos)                 # Amount of videos
        self.pic = pic                          # Pic preview

    def init_extra_data(self, **kwargs):
        self.id = kwargs["id"]                  # id in DB
        self.isChecked = kwargs["isChecked"]    # isChecked in ListWidget
        self.priority = kwargs["priority"]      # priority in ListWidget

    def __str__(self):
        return self.title

    @property
    def is_some_transferred(self):
        import os
        return len(os.listdir(self.path[2])) > 0

    @property
    def path(self):
        from lib.settings import Settings
        import os.path as op
        from lib.utils import Validator
        name = "[" + Validator.path(self.title) + "]"
        output_data = {
            1: op.join(Settings.__main__, name),
            2: op.join(Settings.__transferred__, name),
        }
        return output_data

    @property
    def content(self):
        if self.size == 0:
            video_content = "   * [empty]"
        else:
            video_content = "\n".join([v.content(marker="*", indent="   ") for v in self.videos])
        return "[{size}] {title}\n{videos}".format(
            size=self.size,
            title=self.title,
            videos=video_content
        )

    # TODO: Deprecated
    def expand_videos(self, large=False):

        if self.videos:
            video_list_info = ""
            for i, video in enumerate(self.videos):
                if large:
                    video_list_info += ("{}. {}\n• {}".format(i, video.title, video.url)) + "\n" * (
                            i < len(self.videos) - 1)
                else:
                    video_list_info += ("{}. {}".format(i, video)) + "\n" * (i < len(self.videos) - 1)
        else:
            video_list_info = "[Плейлист пуст]"
        return video_list_info

    # TODO: Deprecated
    def more_info(self):
        from PySide2.QtWidgets import QMessageBox
        # from PySide2.QtGui import QIcon
        # icon = QIcon("lib/favicon.png")
        title = self.title
        full_info = self.expand_videos(large=False)
        from lib.widgets.MainWidget import MainWidget
        mw = MainWidget.get_instance()
        QMessageBox().information(mw, title, full_info, QMessageBox.Ok)
        # TODO: show videos
        # TODO: name
        return True