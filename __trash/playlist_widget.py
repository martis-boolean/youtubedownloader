# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'playlist_widget.ui',
# licensing of 'playlist_widget.ui' applies.
#
# Created: Mon Jul  1 12:46:15 2019
#      by: pyside2-uic  running on PySide2 5.12.3
#
# WARNING! All changes made in this file will be lost!

from PySide2 import QtCore, QtGui, QtWidgets

class Ui_Form(object):
    def setupUi(self, Form):
        Form.setObjectName("Form")
        Form.resize(288, 215)
        Form.setStyleSheet("QHBoxLayout {\n"
"    font-size: 13px;\n"
"    font-family: \"Open Sans\", sans-serif;\n"
"    background-color: rgb(47, 47, 47);\n"
"    color: #fff;\n"
"}\n"
"\n"
"/************ main ****************/\n"
"QWidget {\n"
"    font-size: 13px;\n"
"    font-family: \"Open Sans\", sans-serif;\n"
"    background-color: rgb(47, 47, 47);\n"
"    color: #fff;\n"
"}\n"
"\n"
"/*................. Line Edit .................*/\n"
"QLineEdit {\n"
"    /*font-size/: 11px;*/\n"
"    border: none;\n"
"    color: rgb(200, 200, 200);\n"
"    background-color: rgb(60, 60, 60);\n"
"}\n"
"\n"
"QLineEdit:focus {\n"
"    color: white;\n"
"    border: 1px solid rgb(173, 0, 0);\n"
"}\n"
"/*................. Buttons .................*/\n"
"QPushButton {\n"
"    border: none;\n"
"    font-size: 11px;\n"
"    background-color: rgb(173, 0, 0);\n"
"}\n"
"\n"
"QPushButton:hover {\n"
"    /* background-color: rgb(150, 0, 0); */\n"
"    background-color: rgb(213, 0, 0);\n"
"}\n"
"\n"
"QListWidget {\n"
"    background-color: rgb(42, 42, 42);\n"
"    border: none;\n"
"}\n"
"/************ PlayLists ************/")
        self.groupBox = QtWidgets.QGroupBox(Form)
        self.groupBox.setGeometry(QtCore.QRect(10, 10, 261, 171))
        self.groupBox.setStyleSheet("QGroupBox {\n"
"    border: none;\n"
"    border-bottom: 1px solid rgb(145, 145, 145);\n"
"    background-color: transparent;\n"
"}\n"
"/*background-color: rgba(0, 0, 0, 0.2);*/\n"
"")
        self.groupBox.setTitle("")
        self.groupBox.setObjectName("groupBox")
        self.label_2 = QtWidgets.QLabel(self.groupBox)
        self.label_2.setGeometry(QtCore.QRect(0, 2, 261, 141))
        self.label_2.setStyleSheet("background-color:  rgba(60, 60, 60, 0.5);")
        self.label_2.setText("")
        self.label_2.setObjectName("label_2")
        self.pushButton = QtWidgets.QPushButton(self.groupBox)
        self.pushButton.setGeometry(QtCore.QRect(170, 2, 91, 141))
        self.pushButton.setStyleSheet("QPushButton {\n"
"    padding-top: 30px;\n"
"    font-size: 30px;\n"
"    background-color: rgba(0, 0, 0, 0.5);\n"
"}\n"
"\n"
"QPushButton:hover {\n"
"    background-color: rgba(0, 0, 0, 0.4);\n"
"}")
        self.pushButton.setObjectName("pushButton")
        self.label = QtWidgets.QLabel(self.groupBox)
        self.label.setGeometry(QtCore.QRect(3, 140, 251, 31))
        self.label.setStyleSheet("color: #fff;\n"
"width: 100%;\n"
"background-color: transparent;")
        self.label.setObjectName("label")
        self.label_7 = QtWidgets.QLabel(self.groupBox)
        self.label_7.setGeometry(QtCore.QRect(200, 30, 31, 21))
        self.label_7.setStyleSheet("font-size: 16px;\n"
"background-color: transparent;")
        self.label_7.setTextFormat(QtCore.Qt.PlainText)
        self.label_7.setAlignment(QtCore.Qt.AlignCenter)
        self.label_7.setObjectName("label_7")

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        Form.setWindowTitle(QtWidgets.QApplication.translate("Form", "Form", None, -1))
        self.pushButton.setText(QtWidgets.QApplication.translate("Form", "𝌆", None, -1))
        self.label.setText(QtWidgets.QApplication.translate("Form", "Liked videos", None, -1))
        self.label_7.setText(QtWidgets.QApplication.translate("Form", "3", None, -1))

