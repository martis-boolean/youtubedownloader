# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'main_version.ui',
# licensing of 'main_version.ui' applies.
#
# Created: Tue May 21 18:15:51 2019
#      by: pyside2-uic  running on PySide2 5.12.3
#
# WARNING! All changes made in this file will be lost!

from PySide2 import QtCore, QtGui, QtWidgets

class Ui_YouTubeDownloader(object):
    def setupUi(self, YouTubeDownloader):
        YouTubeDownloader.setObjectName("YouTubeDownloader")
        YouTubeDownloader.resize(1031, 611)
        YouTubeDownloader.setStyleSheet("/************ main_version ****************/\n"
"QWidget {\n"
"    font-size: 14px;\n"
"    font-family: \"Open Sans\", sans-serif;\n"
"    background-color: rgb(47, 47, 47);\n"
"    color: #fff;\n"
"}\n"
"\n"
"/*................. Line Edit .................*/\n"
"QLineEdit {\n"
"    border: none;\n"
"    color: rgb(200, 200, 200);\n"
"    background-color: rgb(60, 60, 60);\n"
"}\n"
"\n"
"QLineEdit:focus {\n"
"    color: white;\n"
"}\n"
"/*................. Buttons .................*/\n"
"QPushButton {\n"
"    border: none;\n"
"    background-color: rgb(193, 0, 0);\n"
"}\n"
"\n"
"QPushButton:hover {\n"
"    background-color: rgb(150, 0, 0);\n"
"}\n"
"\n"
"\n"
"\n"
"/************ step_lable ***********/\n"
"#step_lable {\n"
"    font-size: 16px;\n"
"    font-weight: bold;\n"
"    background-color: rgb(40, 40, 40);\n"
"}\n"
"/************ PlayLists ************/\n"
"QGroupBox {\n"
"    background-color: rgb(57, 57, 57);\n"
"    border: 1px solid rgb(131, 131, 131);\n"
"}\n"
"\n"
"/************ auto_download *****/\n"
"#btn_auto_download {\n"
"    background-color: transparent;\n"
"    color: rgb(117, 157, 243);\n"
"}\n"
"\n"
"#btn_auto_download:focus {\n"
"    /* background-color: transparent;*/\n"
"    color: rgb(147, 187, 255);\n"
"    cursor: pointer;\n"
"}\n"
"")
        self.step_lable = QtWidgets.QLabel(YouTubeDownloader)
        self.step_lable.setGeometry(QtCore.QRect(0, 0, 1031, 41))
        self.step_lable.setAlignment(QtCore.Qt.AlignCenter)
        self.step_lable.setObjectName("step_lable")
        self.listView = QtWidgets.QListView(YouTubeDownloader)
        self.listView.setGeometry(QtCore.QRect(0, 40, 291, 571))
        self.listView.setObjectName("listView")
        self.verticalScrollBar = QtWidgets.QScrollBar(YouTubeDownloader)
        self.verticalScrollBar.setGeometry(QtCore.QRect(290, 40, 20, 561))
        self.verticalScrollBar.setOrientation(QtCore.Qt.Vertical)
        self.verticalScrollBar.setObjectName("verticalScrollBar")
        self.label_playlists = QtWidgets.QLabel(YouTubeDownloader)
        self.label_playlists.setGeometry(QtCore.QRect(330, 70, 151, 21))
        self.label_playlists.setObjectName("label_playlists")
        self.line = QtWidgets.QFrame(YouTubeDownloader)
        self.line.setGeometry(QtCore.QRect(310, 340, 711, 20))
        self.line.setFrameShape(QtWidgets.QFrame.HLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line.setObjectName("line")
        self.groupBox = QtWidgets.QGroupBox(YouTubeDownloader)
        self.groupBox.setGeometry(QtCore.QRect(30, 70, 241, 141))
        self.groupBox.setAlignment(QtCore.Qt.AlignCenter)
        self.groupBox.setObjectName("groupBox")
        self.groupBox_2 = QtWidgets.QGroupBox(YouTubeDownloader)
        self.groupBox_2.setGeometry(QtCore.QRect(30, 230, 241, 141))
        self.groupBox_2.setAlignment(QtCore.Qt.AlignCenter)
        self.groupBox_2.setObjectName("groupBox_2")
        self.groupBox_3 = QtWidgets.QGroupBox(YouTubeDownloader)
        self.groupBox_3.setGeometry(QtCore.QRect(30, 400, 241, 141))
        self.groupBox_3.setAlignment(QtCore.Qt.AlignCenter)
        self.groupBox_3.setObjectName("groupBox_3")
        self.line_edit_playlists_file = QtWidgets.QLineEdit(YouTubeDownloader)
        self.line_edit_playlists_file.setGeometry(QtCore.QRect(490, 70, 441, 21))
        self.line_edit_playlists_file.setObjectName("line_edit_playlists_file")
        self.btn_channel = QtWidgets.QPushButton(YouTubeDownloader)
        self.btn_channel.setGeometry(QtCore.QRect(940, 70, 31, 23))
        self.btn_channel.setObjectName("btn_channel")
        self.btn_browse_list = QtWidgets.QPushButton(YouTubeDownloader)
        self.btn_browse_list.setGeometry(QtCore.QRect(980, 70, 31, 23))
        self.btn_browse_list.setObjectName("btn_browse_list")
        self.line_edit_folder = QtWidgets.QLineEdit(YouTubeDownloader)
        self.line_edit_folder.setGeometry(QtCore.QRect(490, 120, 441, 21))
        self.line_edit_folder.setObjectName("line_edit_folder")
        self.label_folder = QtWidgets.QLabel(YouTubeDownloader)
        self.label_folder.setGeometry(QtCore.QRect(330, 120, 151, 21))
        self.label_folder.setObjectName("label_folder")
        self.btn_browse_folder = QtWidgets.QPushButton(YouTubeDownloader)
        self.btn_browse_folder.setGeometry(QtCore.QRect(940, 120, 71, 23))
        self.btn_browse_folder.setObjectName("btn_browse_folder")
        self.btn_browse_logfile = QtWidgets.QPushButton(YouTubeDownloader)
        self.btn_browse_logfile.setGeometry(QtCore.QRect(940, 370, 71, 23))
        self.btn_browse_logfile.setObjectName("btn_browse_logfile")
        self.line_edit_logfile = QtWidgets.QLineEdit(YouTubeDownloader)
        self.line_edit_logfile.setGeometry(QtCore.QRect(490, 370, 441, 21))
        self.line_edit_logfile.setObjectName("line_edit_logfile")
        self.label_logfile = QtWidgets.QLabel(YouTubeDownloader)
        self.label_logfile.setGeometry(QtCore.QRect(330, 370, 151, 21))
        self.label_logfile.setObjectName("label_logfile")
        self.btn_manual_download = QtWidgets.QPushButton(YouTubeDownloader)
        self.btn_manual_download.setGeometry(QtCore.QRect(550, 510, 211, 51))
        self.btn_manual_download.setObjectName("btn_manual_download")
        self.btn_auto_download = QtWidgets.QPushButton(YouTubeDownloader)
        self.btn_auto_download.setGeometry(QtCore.QRect(510, 570, 291, 21))
        self.btn_auto_download.setObjectName("btn_auto_download")

        self.retranslateUi(YouTubeDownloader)
        QtCore.QMetaObject.connectSlotsByName(YouTubeDownloader)

    def retranslateUi(self, YouTubeDownloader):
        YouTubeDownloader.setWindowTitle(QtWidgets.QApplication.translate("YouTubeDownloader", "YouTubeDownloader main_version", None, -1))
        self.step_lable.setText(QtWidgets.QApplication.translate("YouTubeDownloader", "1. Настройка плейлистов", None, -1))
        self.label_playlists.setText(QtWidgets.QApplication.translate("YouTubeDownloader", "Список плейлистов", None, -1))
        self.groupBox.setTitle(QtWidgets.QApplication.translate("YouTubeDownloader", "Плейлист 1", None, -1))
        self.groupBox_2.setTitle(QtWidgets.QApplication.translate("YouTubeDownloader", "Плейлист 2", None, -1))
        self.groupBox_3.setTitle(QtWidgets.QApplication.translate("YouTubeDownloader", "Плейлист 3", None, -1))
        self.line_edit_playlists_file.setText(QtWidgets.QApplication.translate("YouTubeDownloader", "../../YouTubeDownloader/playlist_list.txt", None, -1))
        self.btn_channel.setText(QtWidgets.QApplication.translate("YouTubeDownloader", "H", None, -1))
        self.btn_browse_list.setText(QtWidgets.QApplication.translate("YouTubeDownloader", "...", None, -1))
        self.line_edit_folder.setText(QtWidgets.QApplication.translate("YouTubeDownloader", "../../YouTubeDownloader", None, -1))
        self.label_folder.setText(QtWidgets.QApplication.translate("YouTubeDownloader", "Папка для скачивания", None, -1))
        self.btn_browse_folder.setText(QtWidgets.QApplication.translate("YouTubeDownloader", "...", None, -1))
        self.btn_browse_logfile.setText(QtWidgets.QApplication.translate("YouTubeDownloader", "...", None, -1))
        self.line_edit_logfile.setText(QtWidgets.QApplication.translate("YouTubeDownloader", "../../YouTubeDownloader/log.txt", None, -1))
        self.label_logfile.setText(QtWidgets.QApplication.translate("YouTubeDownloader", "Файл логов", None, -1))
        self.btn_manual_download.setText(QtWidgets.QApplication.translate("YouTubeDownloader", "Запустить скачивание", None, -1))
        self.btn_auto_download.setText(QtWidgets.QApplication.translate("YouTubeDownloader", "Настроить автоматическую проверку", None, -1))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    YouTubeDownloader = QtWidgets.QWidget()
    ui = Ui_YouTubeDownloader()
    ui.setupUi(YouTubeDownloader)
    YouTubeDownloader.show()
    sys.exit(app.exec_())

