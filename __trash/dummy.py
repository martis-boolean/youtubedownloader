# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'dummy.ui',
# licensing of 'dummy.ui' applies.
#
# Created: Mon Jul  1 12:46:34 2019
#      by: pyside2-uic  running on PySide2 5.12.3
#
# WARNING! All changes made in this file will be lost!

from PySide2 import QtCore, QtGui, QtWidgets


class Ui_Form(object):
    def setupUi(self, widget):
        widget.setObjectName("Form")
        widget.resize(400, 300)
        self.pushButton = QtWidgets.QPushButton(widget)
        self.pushButton.setGeometry(QtCore.QRect(50, 150, 75, 23))
        self.pushButton.setObjectName("pushButton")
        self.pushButton_2 = QtWidgets.QPushButton(widget)
        self.pushButton_2.setGeometry(QtCore.QRect(130, 150, 75, 23))
        self.pushButton_2.setObjectName("pushButton_2")
        self.pushButton_3 = QtWidgets.QPushButton(widget)
        self.pushButton_3.setGeometry(QtCore.QRect(210, 150, 75, 23))
        self.pushButton_3.setObjectName("pushButton_3")

        # self.retranslateUi(widget)
        QtCore.QMetaObject.connectSlotsByName(widget)

    def retranslateUi(self, widget):
        widget.setWindowTitle(QtWidgets.QApplication.translate("Form", "Form", None, -1))
        self.pushButton.setText(QtWidgets.QApplication.translate("Form", "view", None, -1))
        self.pushButton_2.setText(QtWidgets.QApplication.translate("Form", "models", None, -1))
        self.pushButton_3.setText(QtWidgets.QApplication.translate("Form", "admin", None, -1))


class Widget(QtWidgets.QWidget):
    def __init__(self, name, parent=None):
        super(Widget, self).__init__(parent)
        Ui_Form().setupUi(self)
        # self.row = QHBoxLayout()

        # self.row.addWidget(QLabel(name))
        # self.row.addWidget(QPushButton("view"))
        # self.row.addWidget(QPushButton("select"))

        # self.setLayout(self.row)


def get_widget():
    Form = QtWidgets.QWidget()
    ui = Ui_Form()
    ui.setupUi(Form)
    return Form


if __name__ == "__main__":
    import sys

    app = QtWidgets.QApplication(sys.argv)
    Form = get_widget()
    # Form = QtWidgets.QWidget()
    # ui = Ui_Form()
    # ui.setupUi(Form)
    Form.show()
    sys.exit(app.exec_())
