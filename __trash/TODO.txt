/********** FATAL BUGS ***********/
~ TODO: favicon
~ TODO: FORM, QUEUE import
/********** QUESTIONS ************/
V TODO: FIOS integrate?
. TODO: Locales?
? TODO: AssetsList -> DataBase ?
V TODO: ListWidgetTool ===> Queue
V TODO: Class for tools for init-flex?
? TODO: bind: to DB!!! (ListWidget - PLaylist)
! TODO: DATABASE

~ TODO: not responding, when big file
. TODO: console [time_auto] [app_master_now] [action_message]
V TODO: assets/icon init?
V TODO: init form and more often widgets
V TODO: FORM INIT - NoneType because of importing
? TODO: rewrite main, main_window and modal?
. TODO: lineEdit -> textEdit?


/********** DETAILS **************/
# TODO: parseTool - if contains ...
V TODO: PlaylistFileTool
=> # TODO: Bind pls do DB
V TODO: move if selected
~ TODO: SideBar (Refresh, Expand, Up, Down)
V TODO: Notify Up/down, if success
. TODO: ShortCuts
# TODO: StateCheck  Changed (mark) : DB
? TODO: Loader tool? Download Tool console?
# TODO: Open/Browse exceptions: similar files, signature, format
! TODO: similar playlists!
? TODO: multiple selection
~ TODO: tooltips for all
# TODO: remove from Pl.title: (\/ : ? * | < > ") - folder
# TODO: Modal set Modal
# TODO: MODAL: clean all
# TODO: MODAL: EMPTY PLAYLISTS CHANNEL?
# TODO: MODAL: similar URL
# TODO: MODAL: get_playlists by very big channel (exception)
# TODO: loading (MODAL, MAIN)
# TODO: dragndrop QlistWindow? Move Up/down?
# TODO: videospeeddownload progressbar
# TODO: process speed download (PL) and (VIDEO)
# TODO: AutoNextPlayList enabled

/********** VISUAL ***************/
# TODO: Pic resolution
# TODO: mark downloaded video
# TODO: gradient effect animation for progress bar
~ TODO: QMessageBox: expand
    # TODO: display playlist info
~ TODO: QMessageBox: auto
    # TODO: display: autodownload info
~ TODO: StatusBar Messages
# TODO: EveryAction - EveryIcon
~ TODO: Loading process
    # TODO: pics of pl by url
    # TODO: display download info
    # TODO: display video info
    ~ TODO: What video downloading? Status
    # TODO: Change cursor during download (loading...)
V TODO: Border red in LE
# TODO: PLayList Info Interactive

~ /********** BASE-VERSION *********/
V TODO: StatusBar
V TODO: open buttons
~ TODO: clear selection
~ TODO: MODAL WINDOWS: AutoDownload, Playlist Info
V TODO: open path
V TODO: browse path
V TODO: manual download playlist
V TODO: download by path
V TODO: STRUCTURE

~ /********** MAIN-VERSION *********/
V TODO: when download: create folder
V TODO: default paths
V TODO: playlists by path
V TODO: add by channel or add by direct
# TODO: playlists without repeat
V TODO: move by priority -> to file

# TODO: mark for database
# TODO: Mark checked playlists in DB
# TODO: check already downloaded
# TODO: Check process

V TODO: Add Playlist
V TODO: Remove Playlist
V TODO: Downloading Queue

V TODO: isChecked items to download
V TODO: Checkers instead remove

/********** VERSION 0.5 **********/
~ TODO: Playlist Info -> right
~ TODO: WaitCursor
# TODO: CHANGE INTREFACE AFTER STARTING DOWNLAOD
~ TODO: Playlist pic
~ TODO: Video pic


/********** VERSION 0.6 **********/
# TODO: Real SQL database
# TODO: Binding all operations with DB
# TODO: Fix base details bugs and issues: user usability
# TODO: DB::PlayListTool
# TODO: Base checking (if exists)

/********** VERSION 0.7 **********/
# TODO: Full CheckSystem && MarkSystem
    # TODO: Tests Different cases work
    # TODO: Show status
    # TODO: Find different cases
# TODO: Video Quality


/********** VERSION 0.8 **********/
# TODO: LogSystem: how and how

/********** VERSION 0.9 **********/
# TODO: AutoCheckSystem

/********** VERSION 1.0 **********/
! TODO: TESTING!!!
! TODO: ADD NEW_INTERSECTION PLAYLIST!!!
! TODO: WITHOUT REPEATS!!!
! TODO: COMPARE WITH REQUIREMENTS!!!




#################### TO REMOVE ####################


# exitAction = QtWidgets.QAction(QtGui.QIcon('exit.png'), '&Exit', self)
# exitAction.setShortcut('Ctrl+Q')
# exitAction.setStatusTip('Exit application')
# exitAction.triggered.connect(self.close)


# TOREMOVE: self.init_components
        # Buttons
        self.btn_clear_selection = self.window.findChild(QPushButton, "btn_clear_selection")
        self.btn_pl_refresh = self.window.findChild(QPushButton, "btn_pl_refresh")
        self.btn_move_up = self.window.findChild(QPushButton, "btn_move_up")
        self.btn_move_down = self.window.findChild(QPushButton, "btn_move_down")
        self.btn_expand = self.window.findChild(QPushButton, "btn_expand")
        self.btn_channel = self.window.findChild(QPushButton, "btn_add_by_channel")
        self.btn_open_pl_file = self.window.findChild(QPushButton, "btn_open_pl_file")
        self.btn_open_folder = self.window.findChild(QPushButton, "btn_open_folder")
        self.btn_open_logfile = self.window.findChild(QPushButton, "btn_open_logfile")
        self.btn_browse_pl_file = self.window.findChild(QPushButton, "btn_browse_pl_file")
        self.btn_browse_folder = self.window.findChild(QPushButton, "btn_browse_folder")
        self.btn_browse_logfile = self.window.findChild(QPushButton, "btn_browse_logfile")
        self.btn_manual_download = self.window.findChild(QPushButton, "btn_manual_download")
        self.btn_auto_download = self.window.findChild(QPushButton, "btn_auto_download")
        # Label
        self.step_lable = self.window.findChild(QLabel, "step_label")
        self.label_playlists = self.window.findChild(QLabel, "label_playlists")
        self.label_folder = self.window.findChild(QLabel, "label_folder")
        self.label_logfile = self.window.findChild(QLabel, "label_logfile")
        # LineEdits
        self.le_pl_file = self.window.findChild(QLineEdit, "le_pl_file")
        self.le_folder = self.window.findChild(QLineEdit, "le_folder")
        self.le_logfile = self.window.findChild(QLineEdit, "le_logfile")
        Misc

# TOREMOVE: self.init_buttons
        # self.btn_manual_download.clicked.connect(MH.handler_manual_download)
        # self.btn_auto_download.clicked.connect(MH.handler_auto_download)
        # self.btn_pl_refresh.clicked.connect(MH.handler_pl_refresh)
        # self.btn_add_by_channel.clicked.connect(MH.handler_channel)
        #
        # self.btn_open_pl_file.clicked.connect(MH.handler_open_pl_file)
        # self.btn_open_folder.clicked.connect(MH.handler_open_folder)
        # self.btn_open_logfile.clicked.connect(MH.handler_open_logfile)
        #
        # self.btn_browse_pl_file.clicked.connect(MH.handler_browse_pl_file)
        # self.btn_browse_folder.clicked.connect(MH.handler_browse_folder)
        # self.btn_browse_logfile.clicked.connect(MH.handler_browse_logfile)
        #
        # self.btn_expand.clicked.connect(MH.handler_expand)
        # self.btn_move_up.clicked.connect(MH.handler_move_pl_up)
        # self.btn_move_down.clicked.connect(MH.handler_move_pl_down)
        # self.btn_clear_selection.clicked.connect(MH.handler_clear_selection)
# TOREQOC:
    fname = QFileDialog().getOpenFileName(form, "Open Image", p, "Image Files (*.png *.jpg *.bmp)")
# TOIMPROVE: img view
            url = r"https://sun2.ufanet.userapi.com/c851524/v851524641/12cd62/2Cejcne79Cw.jpg"
        img = QImage()
        data = urllib.request.urlopen(url).read()
        img.loadFromData(data)
        w = self.img_video.width()
        h = self.img_video.height()
        pixmap = QPixmap(img).scaled(w, h, Qt.KeepAspectRatio)
        # self.img_video.setPixmap(QPixmap(img).scaledToWidth(100))
        self.img_video.setPixmap(pixmap)
        # imagemap = QPixmap()
        # imagemap.loadFromData(data)
        # imagemap = QPixmap("assets/playlist_default.png")
        # self.img_video.setPixmap(imagemap.scaled(w, h, Qt.KeepAspectRatio))
